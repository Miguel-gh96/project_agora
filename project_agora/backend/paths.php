<?php

/*
 * Name Project
 */
define('PROJECT', '/project_agora/backend/');

/*
 * $path
 */
$path = $_SERVER['DOCUMENT_ROOT'] . PROJECT;

/*
 * SITE_ROOT
 */
define('SITE_ROOT', $path);

/*
 * SITE_PATH
 */
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . PROJECT);

/*
 * LIBS
 */
define('LIBS', SITE_ROOT . 'libs/');

//CLASSES
define('CLASS_PATH', SITE_ROOT . '../assets/img/');

//log
define('USER_LOG_DIR', SITE_ROOT . 'log/user/Site_User_errors.log.txt');
define('GENERAL_LOG_DIR', SITE_ROOT . 'log/general/Site_General_errors.log.txt');

//model
define('MODEL_PATH', SITE_ROOT . 'model/');


//modules
define('MODULES_PATH', SITE_ROOT . 'modules/');

//resources
define('RESOURCES', SITE_ROOT . 'resources/');
//media
define('MEDIA_PATH', SITE_PATH . 'media/');

//media to upload.inc.php
define('MEDIA_PATH_ROOT', SITE_ROOT . 'media/');

define('MODEL_IMG_CHAT_PATH', SITE_PATH . 'media/img_chat/');
//utils
define('UTILS', SITE_ROOT . 'utils/');


//module login
define('FUNCTIONS_LOGIN', SITE_ROOT . 'modules/login/utils/');
define('MODEL_LOGIN', SITE_ROOT . 'modules/login/model/model/');

//module rooms
define('MODEL_ROOMS', SITE_ROOT . 'modules/rooms/model/model/');


//module crud
define('FUNCTIONS_CRUD', SITE_ROOT . 'modules/crud/utils/');
define('MODEL_CRUD', SITE_ROOT . 'modules/crud/model/model/');


//module users
define('MODEL_USERS', SITE_ROOT . 'modules/users/model/model/');
define('FUNCTIONS_USERS', SITE_ROOT . 'modules/users/utils/');

//amigables
define('URL_AMIGABLES', TRUE);

//Production
define('PRODUCTION', true);
