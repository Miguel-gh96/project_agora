<?php
    function enviar_email($arr)
    {
        $html = '';
        $subject = '';
        $body = '';
        $ruta = '';
        $return = '';

        switch ($arr['type']) {
            case 'alta':
                $subject = 'Tu Alta en Project Agora';
                $ruta = "<a href='".$_SERVER['HTTP_HOST']."/project_agora/#/estado/A".$arr['token']."'>aqu&iacute;</a>";
                $body = '</p>Gracias por unirte a nuestra aplicaci&oacute;n<br> Para finalizar el registro, pulsa '.$ruta.'</p>';
                break;

            case 'modificacion':
                $subject = 'Tu Nuevo Password en Project Agora';
                $ruta = '<a href="'.$_SERVER['HTTP_HOST']."/project_agora/#/estado/recuperar/F".$arr['token'].'">aqu&iacute;</a>';
                $body = 'Para recordar tu password pulsa '.$ruta;
                break;

            case 'contact':
                $subject = 'Tu Peticion a Project_agora ha sido enviada';
                $ruta = "<a href='".$_SERVER['HTTP_HOST']."/project_agora/#/contacto'>aqu&iacute;</a>";
                $body = '<p>Para visitar nuestra web, pulsa '.$ruta.'</p>';
                break;

            case 'admin':
                $subject = $arr['inputSubject'];
                $body = 'inputName: '.$arr['inputName'].'<br>'.
                'inputEmail: '.$arr['inputEmail'].'<br>'.
                'inputSubject: '.$arr['inputSubject'].'<br>'.
                'inputMessage: '.$arr['inputMessage'];
                break;
        }

        $html .= '<html>';
        $html .= '<head>';
        $html .= "<meta charset='utf-8' />
              <style>
                        * {
                            margin: 0;
                            padding: 0;
                        }

                        body {
                            margin: 0 auto;
                            width: 500px; /*No funciona solucionar*/
                            height: 100px;
                        }

                        header {
                            padding: 2px;
                            padding-left: 20px;
                            background-color: black;
                            color: white;
                            opacity: 0.8;
                        }

                        section {
                            padding-top: 50px;
                            padding-left: 50px;
                            margin-top: 3px;
                            margin-bottom: 3px;
                            background-color: ghostwhite;
                            height: 100px;
                        }

                        footer {
                            padding: 2px;
                            padding-left: 20px;
                            background-color: black;
                            color: white;
                            opacity: 0.8;
                        }
                        img {
                            float: right;
                            margin: 4px;
                        }
              </style>";
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<header>';
        //$html .= '<img src="'.IMGEMAIL_PATH.'logo.png" width="100px" />';
        $html .= '<h1>'.$subject.'</h1>';
        $html .= '</header>';
        $html .= '<section>';
        $html .= $body;
        $html .= '</section>';
        $html .= '<footer>';
        $html .= '<p>Enviado por Project_agora</p>';
        $html .= '</footer>';
        $html .= '</body>';
        $html .= '</html>';

        set_error_handler('ErrorHandler');
        try {

            $mail = email::getInstance();
            $mail->name = $arr['inputName'];

            if ($arr['type'] == 'admin') {
                $mail->address = 'project.agora.supp@gmail.com';
            } else {
                $mail->address = $arr['inputEmail'];
            }

            $mail->subject = $subject;
            $mail->body = $html;
        } catch (Exception $e) {
            $return = 0;
        }
        restore_error_handler();

        if ($mail->enviar()) {
            $return = 1;
        } else {
            $return = 0;
        }

        return $return;
    }
