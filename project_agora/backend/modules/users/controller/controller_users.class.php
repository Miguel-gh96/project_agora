<?php

class controller_users {

    public function __construct() {
        include FUNCTIONS_USERS . 'functions_users.inc.php';
        include UTILS . 'upload.inc.php';
        $_SESSION['module'] = 'users';
    }

    /**
     * @api {post} backend/index.php?module=users&function=upload_users upload_users
     * @apiName upload_users
     * @apiGroup Users
     * @apiParam {json} result_avatar
     * @apiPermission subscriber, admin, associated
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *      '$result_avatar': 'avatar'
     *     }
     */
    public function upload_users() {
        //if ((isset($_POST["upload"])) && ($_POST["upload"] == true)) {
        $result_avatar = upload_files();
        $_SESSION['result_avatar'] = $result_avatar;
        echo json_encode($result_avatar);
        exit;
        //}
    }

    /**
     * @api {post} backend/index.php?module=users&function=recoger_datos_users recoger_datos_users
     * @apiName recoger_datos_users
     * @apiGroup Users
     * @apiParam {json} id
     * @apiPermission subscriber, admin, associated
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       'success': true,
     *       'user' = {
     *           'user':'meganeo',
     *           'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',
     *           'name_user':'miguel',
     *           'lastname_user':'Gomez',
     *           'birthday_user':'06/01/1996',
     *           'reg_user':'06/01/2016',
     *           'email_user':'emai@examp.es',
     *           'status_user':'active',
     *           'avatar_user':'./backend/media/default-avatar.png',
     *           'country_user':'ES',
     *           'province_user':'16',
     *           'city_user':'Almonacid Del Marquesado',
     *           'level_user':'bajo',
     *           'type_user':'subscriber',
     *           'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'
     *         }
     *     }
     */
    public function recoger_datos_users() {
        if (isset($_GET['id'])) {
            $user = $_GET['id'];

            set_error_handler('ErrorHandler');
            try {
                $result = loadModel(MODEL_USERS, 'users_model', 'recoger_datos_users', $user);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            if ($result) {
                $jsondata['user'] = $result[0];
                $jsondata['success'] = true;

                echo json_encode($jsondata);
                exit;
            } else {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 404;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
        }
    }

    /**
     * @api {post} backend/index.php?module=users&function=update_users update_users
     * @apiName update_users
     * @apiGroup Users
     * @apiParam {json} data  user data update
     * @apiParam {json} data  pass data update
     * @apiPermission subscriber, admin, associated
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       'success': true,
     *       'user' = {
     *           'user':'meganeo',
     *           'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',
     *           'name_user':'miguel',
     *           'lastname_user':'Gomez',
     *           'birthday_user':'06/01/1996',
     *           'reg_user':'06/01/2016',
     *           'email_user':'emai@examp.es',
     *           'status_user':'active',
     *           'avatar_user':'./backend/media/default-avatar.png',
     *           'country_user':'ES',
     *           'province_user':'16',
     *           'city_user':'Almonacid Del Marquesado',
     *           'level_user':'bajo',
     *           'type_user':'subscriber',
     *           'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'
     *         }
     *     }
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       'success': true
     *     }
     */
    public function update_users() {
        if ((isset($_POST['data'])) && (isset($_POST['data']['email']))) {
            $jsondata = array();
            $result = validate_user($_POST['data']);

            if (empty($_SESSION['result_avatar'])) {
                $_SESSION['result_avatar'] = array('resultado' => true, 'error' => '', 'datos' => $_POST['data']['avatar']);
            }
            $result_avatar = $_SESSION['result_avatar'];

            ///////////////CHECK if exist email/////////////////////////////////////
            $conf_email = array(
                'col' => array('email_user'),
                'pattern' => array($result['datos']['email']),
            );

            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_USERS, 'users_model', 'count_users', $conf_email);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            //if email exist in DB
            if ($exist[0]['total'] != '0') {
                if ($_POST['data']['email_old'] != $result['datos']['email']) {
                    $result['resultado'] = false;
                    $result['error']['email'] = 'El email introducido ya existe.';
                }
            }

            if (($result['resultado']) && ($result_avatar['resultado'])) {
                $arrArgument = array(
                    'columnData_Set' => "name_user = '" . $result['datos']['name'] . "' ,"
                    . " lastname_user = '" . $result['datos']['last_name'] . "', "
                    . " email_user = '" . $result['datos']['email'] . "', "
                    . " birthday_user = '" . $result['datos']['birth_date'] . "', "
                    . " level_user = '" . $result['datos']['en_lvl'] . "', "
                    . " country_user = '" . $result['datos']['country'] . "', "
                    . " province_user = '" . $result['datos']['province'] . "', "
                    . " city_user = '" . $result['datos']['town'] . "', "
                    . " avatar_user = '" . $result_avatar['datos'] . "' ",
                    'columnData_Where' => " user = '" . $result['datos']['user']. "'",
                );

                set_error_handler('ErrorHandler');
                try {
                    //throw new Exception("Error Processing Request", 1);
                    $result1 = loadModel(MODEL_USERS, 'users_model', 'update_users', $arrArgument);
                } catch (Exception $e) {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                    echo json_encode($error_DB);
                    exit;
                }
                restore_error_handler();

                if ($result1) {
                    $jsondata['user']['user'] = $_POST['data']['user'];
                    $jsondata['user']['name_user'] = $result['datos']['name'];
                    $jsondata['user']['lastname_user'] = $result['datos']['last_name'];
                    $jsondata['user']['email_user'] = $result['datos']['email'];
                    $jsondata['user']['birthday_user'] = $result['datos']['birth_date'];
                    $jsondata['user']['level_user'] = $result['datos']['en_lvl'];
                    $jsondata['user']['country_user'] = $result['datos']['country'];
                    $jsondata['user']['province_user'] = $result['datos']['province'];
                    $jsondata['user']['city_user'] = $result['datos']['town'];
                    $jsondata['user']['avatar_user'] = $result_avatar['datos'];
                    $jsondata['user']['status_user'] = $_POST['data']['status_user'];
                    $jsondata['user']['type_user'] = $_POST['data']['type_user'];
                    $jsondata['user']['token'] = $_POST['data']['token'];
                    $jsondata['user']['reg_user'] = $_POST['data']['reg_user'];

                    $jsondata['success'] = true;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                    echo json_encode($error_DB);
                    exit;
                }
            } else {
                $jsondata['success'] = false;
                $jsondata['error'] = $result['error'];
                $jsondata['error']['avatar'] = $result_avatar['error'];

                $jsondata['success1'] = false;
                if ($result_avatar['resultado']) {
                    $jsondata['success1'] = true;
                    $jsondata['img_avatar'] = $result_avatar['datos'];
                }

                echo json_encode($jsondata);
                exit;
            }
        }

        if ((!empty($_POST['password'])) && (!empty($_POST['user']))) {
            $arrArgument = array(
                'columnData_Set' => " pass_user = '" . password_hash($_POST['password'], PASSWORD_BCRYPT) . "' ",
                'columnData_Where' => " user = '" . $_POST['user'] . "'",
            );

            set_error_handler('ErrorHandler');
            try {
                $result1 = loadModel(MODEL_USERS, 'users_model', 'update_users', $arrArgument);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            if ($result1) {
                $jsondata['success'] = true;
                echo json_encode($jsondata);
                exit;
            } else {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
        }
    }

    /**
     * @api {post} backend/index.php?module=users&function=delete_users delete_users
     * @apiName delete_users
     * @apiGroup Users
     * @apiParam {json} delete  url_avatar
     * @apiDescription
     *This is a function used to remove avatar.
     * @apiPermission subscriber, admin, associated
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       ['res':true]
     *     }
     */
    public function delete_users() {
        if (isset($_POST['delete']) && $_POST['delete'] == true) {
            $_SESSION['result_avatar'] = array();
            $result = remove_files();
            if ($result === true) {
                echo json_encode(array('res' => true));
            } else {
                echo json_encode(array('res' => false));
            }
        }
    }

    /**
     * @api {post} backend/index.php?module=users&function=load_countries_users load_countries_users
     * @apiName load_countries_users
     * @apiGroup Users
     * @apiParam {json} load_country
     * @apiPermission subscriber, admin, associated
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       $json
     *     }
     */
    public function load_countries_users() {
        if ((isset($_POST['load_country'])) && ($_POST['load_country'] == true)) {
            $json = array();

            $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';

            try {
                //throw new Exception();
                $json = loadModel(MODEL_USERS, 'users_model', 'obtain_countries', $url);
            } catch (Exception $e) {
                $json = array();
            }

            if ($json) {
                echo $json;
                exit;
            } else {
                $json = 'error';
                echo $json;
                exit;
            }
        }
    }

    /**
     * @api {post} backend/index.php?module=users&function=load_provinces_users load_provinces_users
     * @apiName load_provinces_users
     * @apiGroup Users
     * @apiParam {json} load_provinces
     * @apiPermission subscriber, admin, associated
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *     'provinces': $json
     *     }
     */
    public function load_provinces_users() {
        if ((isset($_POST['load_provinces'])) && ($_POST['load_provinces'] == true)) {
            $jsondata = array();
            $json = array();

            try {
                $json = loadModel(MODEL_USERS, 'users_model', 'obtain_provinces');
            } catch (Exception $e) {
                $json = array();
            }

            if ($json) {
                $jsondata['provinces'] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata['provinces'] = 'error';
                echo json_encode($jsondata);
                exit;
            }
        }
    }

    /**
     * @api {post} backend/index.php?module=users&function=load_towns_users load_towns_users
     * @apiName load_towns_users
     * @apiGroup Users
     * @apiParam {json} idPoblac
     * @apiPermission subscriber, admin, associated
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        'towns': $json
     *     }
     */
    public function load_towns_users() {
        if (isset($_POST['idPoblac'])) {
            $jsondata = array();
            $json = array();

            try {
                $json = loadModel(MODEL_USERS, 'users_model', 'obtain_city', $_POST['idPoblac']);
            } catch (Exception $e) {
                $json = array();
            }
            if ($json) {
                $jsondata['towns'] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $json = array();
                echo json_encode($jsondata);
                exit;
            }
        }
    }

}
