<?php
class users_bll {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = users_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function count_users_BLL($arrArgument) {
        return $this->dao->count_users_DAO($this->db, $arrArgument);
    }

    public function recoger_datos_users_BLL($arrargument) {
        return $this->dao->recoger_datos_users_DAO($this->db, $arrargument);
    }

     public function update_users_BLL($arrArgument) {
        return $this->dao->update_users_DAO($this->db, $arrArgument);
    }

    public function obtain_countries_BLL($url) {
        return $this->dao->obtain_countries_DAO($url);
    }

    public function obtain_provinces_BLL() {
        return $this->dao->obtain_provinces_DAO();
    }

    public function obtain_city_BLL($arrArgument) {
        return $this->dao->obtain_city_DAO($arrArgument);
    }

}
