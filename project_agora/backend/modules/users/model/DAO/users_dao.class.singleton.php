<?php
class users_dao {

    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function recoger_datos_users_DAO($db, $user) {

        $sql = "SELECT * from users WHERE user = '$user'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

      public function update_users_DAO($db,$arrArgument) {
        $columnData_Set = $arrArgument['columnData_Set'];
        $columnData_Where = $arrArgument['columnData_Where'];

        $sql = "UPDATE users SET ".$columnData_Set." WHERE " .$columnData_Where;
        return $db->ejecutar($sql);
    }


    public function obtain_countries_DAO($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $file_contents = curl_exec($ch);
        curl_close($ch);

        return ($file_contents) ? $file_contents : FALSE;
    }

    public function obtain_provinces_DAO() {
        $json = array();
        $tmp = array();

        $provincias = simplexml_load_file(RESOURCES . "provinciasypoblaciones.xml");
        $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");

        for ($i = 0; $i < count($result); $i+=2) {
            $e = $i + 1;
            $provincia = $result[$e];

            $tmp = array(
                '_id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
        }
        return $json;
    }

    public function obtain_city_DAO($arrArgument) {
        $json = array();
        $tmp = array();

        $filter = (string) $arrArgument;
        $xml = simplexml_load_file(RESOURCES . 'provinciasypoblaciones.xml');
        $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

        for ($i = 0; $i < count($result[0]); $i++) {
            $tmp = array(
                '__cdata' => (string) $result[0]->localidad[$i]
            );
            array_push($json, $tmp);
        }
        return $json;
    }

    /*
      General function to count in DB
            --> @params $db, $arrArgument --> pattern
                                          --> col--> name of column where you want check
            --> @return number
    */
    public function count_users_DAO($db,$arrArgument) {
        $sql = "SELECT COUNT(*) AS total FROM users WHERE ";

        for ($i = 0; $i < count($arrArgument['col']);$i++){
            $sql .= $arrArgument['col'][$i]." = '".$arrArgument['pattern'][$i]."' ";

            if($i < (count($arrArgument['col']) -1 )){
                $sql .="AND ";
            }
        }

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

}
