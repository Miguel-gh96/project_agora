<?php
class users_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = users_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function count_users($arrArgument) {
        return $this->bll->count_users_BLL($arrArgument);
    }

     public function recoger_datos_users($arrargument) {
        return $this->bll->recoger_datos_users_BLL($arrargument);
    }

    public function update_users($arrArgument) {
        return $this->bll->update_users_BLL($arrArgument);
    }

    public function obtain_countries($url) {
        return $this->bll->obtain_countries_BLL($url);
    }

    public function obtain_provinces() {
        return $this->bll->obtain_provinces_BLL();
    }

    public function obtain_city($arrArgument) {
        return $this->bll->obtain_city_BLL($arrArgument);
    }

}
