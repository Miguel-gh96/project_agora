<?php

function validate_user($value)
{
    $error = array();
    $valido = true;
    $filtro = array(
        'name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[A-Za-z-á-é-í-ó-ú\s]{2,30}$/'),
        ),
        'last_name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[A-Za-z-á-é-í-ó-ú\s]{2,30}$/'),
        ),
        'birth_date' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/'),
        ),
        'pass' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9a-zA-Z]{6,32}$/'),
        ),
        'email' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'valida_email',
        ),
        'country' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-zA-Z_]*$/'),
        ),
        'province' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-zA-Z0-9, _]*$/'),
        ),
        'town' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'validate_town',
        ),
    );

    $resultado = filter_var_array($value, $filtro);
    //no filter
    $resultado['en_lvl'] = $value['en_lvl'];
    $resultado['user'] = $value['user'];

    if ($resultado['en_lvl'] === 'Select level') {
        $error['en_lvl'] = "You haven't select lvl.";
        $valido = false;
    }

    if ($resultado != '' && $resultado) {
        if ($value['name'] != '') {
            if (!$resultado['name']) {
                $error['name'] = 'Name must be 2 to 30 letters';
                $valido = false;
            }
        }

        if ($value['last_name'] != '') {
            if (!$resultado['last_name']) {
                $error['last_name'] = 'Last name must be 2 to 30 letters';
                $valido = false;
            }
        }

        if (!$resultado['email']) {
            $error['email'] = 'error format email (example@example.com)';
            $valido = false;
        }

        if ($value['birth_date'] != '') {
            if ($resultado['birth_date']) {
                //validate to user's over 16
              $dates = validateAge($resultado['birth_date']);

                if (!$dates) {
                    $error['birth_date'] = 'Debes ser mayor de 16.';
                    $valido = false;
                }
            }

            if (!$resultado['birth_date']) {
                $error['birth_date'] = 'error format date (mm/dd/yyyy)';
                $valido = false;
            }
        }

        if (!$resultado['country']) {
            $error['country'] = 'Seleccione un país correcto';
            $resultado['country'] = $value['country'];
            $valido = false;
        }

        if (!$resultado['province']) {
            $error['province'] = 'Seleccione una provincia correcta';
            $resultado['province'] = $value['province'];
            $valido = false;
        }

        if (!$resultado['town']) {
            $error['town'] = 'Seleccione una ciudad correcta';
            $resultado['town'] = $value['town'];
            $valido = false;
        }
    } else {
        $valido = false;
    };

    return $return = array('resultado' => $valido, 'error' => $error, 'datos' => $resultado);
}

function valida_dates($start_days, $dayslight)
{
    $start_day = date('m/d/Y', strtotime($start_days));
    $daylight = date('m/d/Y', strtotime($dayslight));

    list($mes_one, $dia_one, $anio_one) = split('/', $start_day);
    list($mes_two, $dia_two, $anio_two) = split('/', $daylight);

    $dateOne = new DateTime($anio_one.'-'.$mes_one.'-'.$dia_one);
    $dateTwo = new DateTime($anio_two.'-'.$mes_two.'-'.$dia_two);

    if ($dateOne <= $dateTwo) {
        return true;
    }

    return false;
}

// validate birthday
function validateAge($birthday, $age = 16)
{
    // $birthday can be UNIX_TIMESTAMP or just a string-date.
    if (is_string($birthday)) {
        $birthday = strtotime($birthday);
    }

    // check
    // 31536000 is the number of seconds in a 365 days year.
    if (time() - $birthday < $age * 31536000) {
        return false;
    }

    return true;
}

//validate email
function valida_email($email)
{
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if (filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^.{5,50}$/')))) {
            return $email;
        }
    }

    return false;
}

function validate_town($town)
{
    $town = filter_var($town, FILTER_SANITIZE_STRING);

    return $town;
}
