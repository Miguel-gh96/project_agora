<?php
class login_bll {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = login_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function get_user_BLL($arrargument) {
        return $this->dao->get_user_DAO($this->db, $arrargument);
    }

    public function create_user_BLL($arrArgument) {
        return $this->dao->create_user_DAO($this->db, $arrArgument);
    }

    public function count_users_BLL($arrArgument) {
        return $this->dao->count_users_DAO($this->db, $arrArgument);
    }

    public function update_users_BLL($arrArgument) {
        return $this->dao->update_users_DAO($this->db, $arrArgument);
    }
    

}
