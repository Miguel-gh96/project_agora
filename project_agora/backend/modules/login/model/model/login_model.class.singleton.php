<?php
class login_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = login_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function get_user($arrargument) {
        return $this->bll->get_user_BLL($arrargument);
    }

    public function create_user($arrArgument) {
        return $this->bll->create_user_BLL($arrArgument);
    }

    public function count_users($arrArgument) {
        return $this->bll->count_users_BLL($arrArgument);
    }

    public function update_users($arrArgument) {
        return $this->bll->update_users_BLL($arrArgument);
    }

}
