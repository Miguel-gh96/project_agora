<?php

class login_dao {

    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    /* Get User in DB to logIn
        --> @params $db, $arrArgument --> array patterns
                                      --> array cols
        --> @return user
    */
    public function get_user_DAO($db, $arrargument) {

      $sql = "SELECT * FROM users WHERE ";

      for ($i = 0; $i < count($arrargument['cols']);$i++){
          $sql .= $arrargument['cols'][$i]." = '".$arrargument['pattern'][$i]."' ";

          if($i < (count($arrargument['cols']) -1 )){
              $sql .="AND ";
          }


      }

      $stmt = $db->ejecutar($sql);
      return $db->listar($stmt);
    }


    /* Create User
        --> @params $db, $arrArgument --> array patterns
                                      --> array cols
        --> @return true/false
    */
    public function create_user_DAO($db, $arrArgument) {

          $cols ="";
          $pattern = "";

          for ($i = 0; $i < count($arrArgument['cols']);$i++){

              $cols .= $arrArgument['cols'][$i];
              $pattern .= "'".$arrArgument['pattern'][$i]."'";

              if($i < (count($arrArgument['cols']) -1 )){
                  $cols .= ", ";
                  $pattern .=", ";
              }

          }

        $sql = "INSERT INTO users (".$cols.") VALUES (".$pattern.")";

        return $db->ejecutar($sql);
    }


    /*
      General function to count in DB
            --> @params $db, $arrArgument --> pattern
                                          --> col--> name of column where you want check
            --> @return number
    */
    public function count_users_DAO($db,$arrArgument) {
        $sql = "SELECT COUNT(*) AS total FROM users WHERE ";

        for ($i = 0; $i < count($arrArgument['col']);$i++){
            $sql .= $arrArgument['col'][$i]." = '".$arrArgument['pattern'][$i]."' ";

            if($i < (count($arrArgument['col']) -1 )){
                $sql .="AND ";
            }
        }

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    /*
      Upadate Users
            --> @params $db, $arrArgument --> columnData_Set
                                          --> columnData_Where
            --> @return true/false
    */
    public function update_users_DAO($db,$arrArgument) {
        $columnData_Set = $arrArgument['columnData_Set'];
        $columnData_Where = $arrArgument['columnData_Where'];

        $sql = "UPDATE users SET ".$columnData_Set." WHERE " .$columnData_Where;
        return $db->ejecutar($sql);
    }

}
