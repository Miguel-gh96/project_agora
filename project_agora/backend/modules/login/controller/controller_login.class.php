<?php

class controller_login
{
    public function __construct()
    {
        include FUNCTIONS_LOGIN.'functions_login.inc.php';
        include UTILS.'upload.inc.php';
        $_SESSION['module'] = 'login';
    }

/**
 * @api {post} backend/index.php?module=login&function=nuevo SignUp
 * @apiName SignUp
 * @apiGroup login
 * @apiPermission none
 * @apiParam {json} jsondata data new User.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        'success' = true;
 *     }
 */
    //////////////SIGNIN USER////////////////////////////////////////
    public function signUp()
    {
        if ((isset($_POST['data']))) {
            $jsondata = array();
            //$usersJSON = json_decode($_POST[0], true);
            $result = validate_user($_POST['data']);

            ///////////////CHECK if exist email/////////////////////////////////////
            $conf_email = array(
                'col' => array('email_user'),
                'pattern' => array($result['datos']['email']),
            );

            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_LOGIN, 'login_model', 'count_users', $conf_email);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            //if email exist in DB
            if ($exist[0]['total'] != '0') {
                $result['resultado'] = false;
                $result['error']['email'] = 'El email introducido ya existe.';
            }

            ///////////////CHECK if exist user/////////////////////////////////////
            $conf_user = array(
                'col' => array('user'),
                'pattern' => array($result['datos']['user']),
            );

            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_LOGIN, 'login_model', 'count_users', $conf_user);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            //if user exist in DB
            if ($exist[0]['total'] != '0') {
                $result['resultado'] = false;
                $result['error']['user'] = 'El usuario introducido ya existe.';
            }

            if (($result['resultado'])) {
                //Obtain date sign up
                $date = date('m/d/Y', time());

                //Obtain token to active user
                $token = md5(uniqid(microtime(), true));

                $grav_url1 = get_gravatar($result['datos']['email'], $s = 400, $d = 'identicon', $r = 'g', $img = false, $atts = array());
                /*
                  status:
                  inactive
                  active

                  type:
                  user
                  user_associated
                  admin
                 */

                $arrArgument = array(
                    'cols' => array(
                        'user',
                        'pass_user',
                        'name_user',
                        'lastname_user',
                        'birthday_user',
                        'reg_user',
                        'email_user',
                        'status_user',
                        'avatar_user',
                        'country_user',
                        'province_user',
                        'city_user',
                        'level_user',
                        'type_user',
                        'token',
                    ),
                    'pattern' => array(
                        $result['datos']['user'],
                        password_hash($result['datos']['pass'], PASSWORD_BCRYPT),
                        '',
                        '',
                        '',
                        $date,
                        $result['datos']['email'],
                        'inactive',
                        $grav_url1,
                        '',
                        '',
                        '',
                        ($result['datos']['level']),
                        'subscriber',
                        $token,
                    ),
                );

                /////////////////insert into BD////////////////////////
                $arrValue = false;

                set_error_handler('ErrorHandler');

                try {
                    $arrValue = loadModel(MODEL_LOGIN, 'login_model', 'create_user', $arrArgument);
                } catch (Exception $e) {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                    echo json_encode($error_DB);
                    exit;
                }
                restore_error_handler();

                if ($arrValue) {

                    //////SEND EMAIL///////
                    $arrArgument2 = array(
                        'type' => 'alta',
                        'token' => $token,
                        'inputName' => $arrArgument['pattern'][0],
                        'inputEmail' => $arrArgument['pattern'][6],
                        'inputSubject' => 'alta',
                        'inputMessage' => 'alta',
                    );

                    $envio = enviar_email($arrArgument2);

                    //doesn't send email
                    if ($envio === 0) {
                        $error_DB['success'] = false;
                        $error_DB['error_type'] = 500;
                        $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                        echo json_encode($error_DB);
                        exit;
                    }
                } else {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                    echo json_encode($error_DB);
                    exit;
                }

                $jsondata['success'] = true;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata['success'] = false;
                $jsondata['error'] = $result['error'];
                header('HTTP/1.0 400 Bad error', true, 404);
                echo json_encode($jsondata);
                //die();
            }
        }
    }

    /**
     * @api {post} backend/index.php?module=login&function=exist_user SignIn
     * @apiName SignIn_enter
     * @apiGroup login
     * @apiParam {json} jsondata Name user and pass user.
     * @apiPermission none
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        'success' = true,
     *        'user' = {
     *           'user':'meganeo',
     *           'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',
     *           'name_user':'miguel',
     *           'lastname_user':'Gomez',
     *           'birthday_user':'06/01/1996',
     *           'reg_user':'06/01/2016',
     *           'email_user':'emai@examp.es',
     *           'status_user':'active',
     *           'avatar_user':'./backend/media/default-avatar.png',
     *           'country_user':'ES',
     *           'province_user':'16',
     *           'city_user':'Almonacid Del Marquesado',
     *           'level_user':'bajo',
     *           'type_user':'subscriber',
     *           'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'
     *         }
     *     }
     */
    public function signIn_enter()
    {
        if (!empty($_POST['user']) && !empty($_POST['pass'])) {
            $user = ($_POST['user']);
            $pass_no_crypt = ($_POST['pass']);

            //password verify
            $pass = password_hash($pass_no_crypt, PASSWORD_BCRYPT);

            $arrargument = array(
                'cols' => array(
                    'user',
                    'status_user',
                ),
                'pattern' => array(
                    $user,
                    'active',
                ),
            );

            set_error_handler('ErrorHandler');
            try {
                $result1 = loadModel(MODEL_LOGIN, 'login_model', 'get_user', $arrargument);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            if ($result1) {
                if (password_verify($pass_no_crypt, $result1[0]['pass_user'])) {

                    $jsondata['success'] = true;
                    $jsondata['user'] = $result1[0];
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 404;
                    $error_DB['error_message'] = 'Datos incorrectos.';
                    echo json_encode($error_DB);
                    exit;
                }
            } else {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 404;
                $error_DB['error_message'] = 'Datos incorrectos.';
                echo json_encode($error_DB);
                exit;
            }
        }
    }

    /**
     * @api {get} backend/index.php?module=login&function=activar&id=4ab160816a8225bcd7fcb3a7fa3b4d42 activate_user
     * @apiName activate_user
     * @apiGroup login
     * @apiParam {json} ID  token to activate or recover password.
     * @apiPermission none
     * @apiSuccessExample {json} jsondata Success-Response activate User:
     *     HTTP/1.1 200 OK
     *     {
     *        'success' = true,
     *        'user' = {
     *           'user':'meganeo',
     *           'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',
     *           'name_user':'miguel',
     *           'lastname_user':'Gomez',
     *           'birthday_user':'06/01/1996',
     *           'reg_user':'06/01/2016',
     *           'email_user':'emai@examp.es',
     *           'status_user':'active',
     *           'avatar_user':'./backend/media/default-avatar.png',
     *           'country_user':'ES',
     *           'province_user':'16',
     *           'city_user':'Almonacid Del Marquesado',
     *           'level_user':'bajo',
     *           'type_user':'subscriber',
     *           'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'
     *         }
     *     }
     * @apiSuccessExample {json} jsondata Success-Response recover Password:
     *     HTTP/1.1 200 OK
     *     {
     *        'success' = true,
     *     }
     */
    public function activate_user()
    {
        if (isset($_GET['id'])) {
            $token = substr($_GET['id'], 1);
            $controll = substr($_GET['id'], 0, 1);

            if ($controll == 'A') {

                ///ACTIVE USER//////
                $arrArgument = array(
                    'columnData_Set' => " status_user = 'active' ",
                    'columnData_Where' => " token = '".$token."'",
                );

                set_error_handler('ErrorHandler');
                try {
                    $exist = loadModel(MODEL_LOGIN, 'login_model', 'update_users', $arrArgument);
                } catch (Exception $e) {
                    ///TEMPLATE ERROR//////////////
                    $error_DB['error_DB']['error_type'] = 500;
                    $error_DB['error_DB']['error_message'] = 'Hubo un problema con el servidor, intentalo más tarde.';
                    $error_DB['redirect'] = '';
                    echo json_encode($error_DB);
                    exit;
                }
                restore_error_handler();

                if ($exist) {

                    //Obtain User////
                    $arrargument = array(
                        'cols' => array(
                            'token',
                        ),
                        'pattern' => array(
                            $token,
                        ),
                    );

                    set_error_handler('ErrorHandler');
                    try {
                        $result1 = loadModel(MODEL_LOGIN, 'login_model', 'get_user', $arrargument);
                    } catch (Exception $e) {
                        ///TEMPLATE ERROR//////////////
                        $error_DB['error_DB']['error_type'] = 500;
                        $error_DB['error_DB']['error_message'] = 'Hubo un problema con el servidor, intentalo más tarde.';
                        $error_DB['redirect'] = '';
                        echo json_encode($error_DB);
                        exit;
                    }
                    restore_error_handler();
                    if ($result1) {
                        $jsondata['success'] = true;
                        //$callback = amigable('?module=rooms&function=list_rooms', true);
                        $jsondata['user'] = $result1[0];
                        echo json_encode($jsondata);
                        exit;
                    } else {
                        //user doesn't exist
                        $error_DB['error_DB']['error_type'] = 500;
                        $error_DB['error_DB']['error_message'] = 'Hubo un problema con el servidor, intentalo más tarde.';
                        $error_DB['redirect'] = '';
                        echo json_encode($error_DB);
                        exit;
                    }
                } else {
                    ///TEMPLATE ERROR//////////////
                    $error_DB['error_DB']['error_type'] = 500;
                    $error_DB['error_DB']['error_message'] = 'Hubo un problema con el servidor, intentalo más tarde.';
                    $error_DB['redirect'] = '';
                    echo json_encode($error_DB);
                    exit;
                }
            } elseif ($controll == 'F') {
                /////////RECOVER PASSWORD ///////
                //Obtain User////
                $arrargument = array(
                    'cols' => array(
                        'token',
                    ),
                    'pattern' => array(
                        $token,
                    ),
                );

                set_error_handler('ErrorHandler');
                try {
                    $result1 = loadModel(MODEL_LOGIN, 'login_model', 'get_user', $arrargument);
                } catch (Exception $e) {
                    ///TEMPLATE ERROR//////////////
                    $error_DB['error_DB']['error_type'] = 500;
                    $error_DB['error_DB']['error_message'] = 'Hubo un problema con el servidor, intentalo más tarde.';
                    $error_DB['redirect'] = '';
                    echo json_encode($error_DB);
                    exit;
                }
                restore_error_handler();

                if ($result1) {
                    $jsondata['user'] = $result1[0];
                    $jsondata['success'] = true;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    //user doesn't exist
                    $jsondata['success'] = false;
                    $jsondata['redirect'] = '';
                    echo json_encode($jsondata);
                    exit;
                }
            } else {
                $jsondata['success'] = false;
                $jsondata['redirect'] = '';
                echo json_encode($jsondata);
                exit;
            }
        } else {
            $jsondata['success'] = false;
            $jsondata['redirect'] = '';
            echo json_encode($jsondata);
            exit;
        }
    }

    /**
     * @api {get} backend/index.php?module=login&function=exist_email&id=example@exampl.es recover_pass
     * @apiName recover_pass
     * @apiGroup login
     * @apiParam {string} Email  Email to confirm account.
     * @apiPermission none
     * @apiSuccessExample {json} jsondata Success-Response recover Password:
     *     HTTP/1.1 200 OK
     *     {
     *        'success' = true,
     *     }
     */
    public function recover_pass()
    {

        if (!empty($_GET['id'])) {
            $email = ($_GET['id']);
            ///////////////CHECK if exist email/////////////////////////////////////
            $conf_email = array(
                'col' => array('email_user'),
                'pattern' => array($email),
            );

            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_LOGIN, 'login_model', 'count_users', $conf_email);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            if ($exist[0]['total'] === '1') {
                $token = md5(uniqid(microtime(), true));

                ///Update token//////
                $arrArgument = array(
                    'columnData_Set' => " token = '".$token."' ",
                    'columnData_Where' => " email_user = '".$email."'",
                );

                set_error_handler('ErrorHandler');
                try {
                    $exist = loadModel(MODEL_LOGIN, 'login_model', 'update_users', $arrArgument);
                } catch (Exception $e) {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                    echo json_encode($error_DB);
                    exit;
                }
                restore_error_handler();

                if ($exist) {
                    ///PRUEBA PARA COMPROBAR Q FUNCIONA/////
                    $arrArgument2 = array(
                        'type' => 'modificacion',
                        'token' => $token,
                        'inputName' => 'Usuario',
                        'inputEmail' => $email,
                        'inputSubject' => 'Recuperar Contraseña',
                        'inputMessage' => 'Recuperar Contraseña',
                    );

                    $envio = enviar_email($arrArgument2);

                    $jsondata['success'] = true;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 404;
                    $error_DB['error_message'] = 'El email introducido no es válido.';
                    echo json_encode($error_DB);
                    exit;
                }
            } else {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 404;
                $error_DB['error_message'] = 'El email introducido no es válido.';
                echo json_encode($error_DB);
                exit;
            }
        }
    }

    /**
     * @api {post} backend/index.php?module=login&function=exist_email update_pass
     * @apiName update_pass
     * @apiGroup login
     * @apiPermission subscriber,associate,admin
     *
     * @apiParam {json} data  Pass and token  to change pass  account.
     *
     * @apiSuccessExample {json} jsondata Success-Response recover Password:
     *     HTTP/1.1 200 OK
     *     {
     *        'success' = true
     *     }
     */
    public function update_pass()
    {

        if ((!empty($_POST['id']))&&(!empty($_POST['token']))) {
            $password = password_hash($_POST['id'], PASSWORD_BCRYPT);

            $arrArgument = array(
                'columnData_Set' => " pass_user = '".$password."' ",
                'columnData_Where' => " token = '".$_POST['token']."'",
            );

            set_error_handler('ErrorHandler');
            try {
                $result1 = loadModel(MODEL_LOGIN, 'login_model', 'update_users', $arrArgument);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            if ($result1) {
                $jsondata['success'] = true;
                echo json_encode($jsondata);
                exit;
            } else {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
        }
    }

   /**
    * @api {post} backend/index.php?module=login&function=social_facebook social_facebook
    * @apiName social_facebook
    * @apiGroup login
    * @apiPermission subscriber,associate,admin
    * @apiParam {string} Pass  Pass to change pass  account.
    *
    *
    * @apiSuccessExample {json} jsondata Success-Response recover Password:
    *     HTTP/1.1 200 OK
    *     {
    *        'success' = true,
    *        'redirect' = '/salas',
    *        'user' = {
    *           'user':'meganeo',
    *           'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',
    *           'name_user':'miguel',
    *           'lastname_user':'Gomez',
    *           'birthday_user':'06/01/1996',
    *           'reg_user':'06/01/2016',
    *           'email_user':'emai@examp.es',
    *           'status_user':'active',
    *           'avatar_user':'./backend/media/default-avatar.png',
    *           'country_user':'ES',
    *           'province_user':'16',
    *           'city_user':'Almonacid Del Marquesado',
    *           'level_user':'bajo',
    *           'type_user':'subscriber',
    *           'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'
    *         }
    *     }
    */
   public function social_facebook()
   {
       if (!empty($_POST['data'])) {
           $user = ($_POST['data']['user']);
           $email = ($_POST['data']['email']);
           $name = ($_POST['data']['first_name']);
           $lastname = ($_POST['data']['last_name']);
           $avatar = 'http://graph.facebook.com/'.($_POST['data']['id']).'/picture';
        //$avatar = ($_POST['data']['name']['data']['url']);

        $arrargument = array(
            'cols' => array(
                'user',
                'email_user',
            ),
            'pattern' => array(
                $user,
                $email,
            ),
        );
           set_error_handler('ErrorHandler');
           try {
               $result1 = loadModel(MODEL_LOGIN, 'login_model', 'get_user', $arrargument);
           } catch (Exception $e) {
               $error_DB['success'] = false;
               $error_DB['error_type'] = 500;
               $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
               echo json_encode($error_DB);
               exit;
           }
           restore_error_handler();

           if ($result1) {
               $jsondata['user'] = $result1[0];

               $callback = '/salas';

               $jsondata['success'] = true;
               $jsondata['redirect'] = $callback;
               echo json_encode($jsondata);
               exit;
           } else {
               $date = date('m/d/Y', time());

               $arrargument = array(
                'cols' => array(
                    'user',
                    'pass_user',
                    'name_user',
                    'lastname_user',
                    'birthday_user',
                    'reg_user',
                    'email_user',
                    'status_user',
                    'avatar_user',
                    'country_user',
                    'province_user',
                    'city_user',
                    'level_user',
                    'type_user',
                    'token',
                ),
                'pattern' => array(
                    $user,
                    '',
                    $name,
                    $lastname,
                    '',
                    $date,
                    $email,
                    'active',
                    $avatar,
                    'default_country',
                    'default_province',
                    'default_town',
                    'bajo',
                    'subscriber',
                    '',
                ),
            );

               set_error_handler('ErrorHandler');
               try {
                   $arrValue = loadModel(MODEL_LOGIN, 'login_model', 'create_user', $arrargument);
               } catch (Exception $e) {
                   $error_DB['success'] = false;
                   $error_DB['error_type'] = 500;
                   $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                   echo json_encode($error_DB);
                   exit;
               }
               restore_error_handler();

               if ($arrValue) {
                   set_error_handler('ErrorHandler');
                   try {
                       $result1 = loadModel(MODEL_LOGIN, 'login_model', 'get_user', $arrargument);
                   } catch (Exception $e) {
                       $error_DB['success'] = false;
                       $error_DB['error_type'] = 500;
                       $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                       echo json_encode($error_DB);
                       exit;
                   }
                   restore_error_handler();

                   if ($result1) {
                       $jsondata['user'] = $result1[0];

                       $callback = '/salas';
                       $jsondata['success'] = true;
                       $jsondata['redirect'] = $callback;
                       echo json_encode($jsondata);
                       exit;
                   } else {
                       $error_DB['success'] = false;
                       $error_DB['error_type'] = 500;
                       $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                       echo json_encode($error_DB);
                       exit;
                   }
               } else {
                   $error_DB['success'] = false;
                   $error_DB['error_type'] = 500;
                   $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                   echo json_encode($error_DB);
                   exit;
               }
           }
       } else {
           $error_DB['success'] = false;
           $error_DB['error_type'] = 404;
           $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
           echo json_encode($error_DB);
           exit;
       }
   }

    /**
     * @api {post} backend/index.php?module=login&function=twitter_return twitter_return
     * @apiName twitter_return
     * @apiGroup login
     * @apiPermission none
     * @apiParam {json} user User data.
     *
     * @apiSuccessExample {json} jsondata Success-Response recover Password:
     *     HTTP/1.1 200 OK
     *     {
     *        'success' = true,
     *        'redirect' = '/salas',
     *        'user' = {
     *           'user':'meganeo',
     *           'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',
     *           'name_user':'miguel',
     *           'lastname_user':'Gomez',
     *           'birthday_user':'06/01/1996',
     *           'reg_user':'06/01/2016',
     *           'email_user':'emai@examp.es',
     *           'status_user':'active',
     *           'avatar_user':'./backend/media/default-avatar.png',
     *           'country_user':'ES',
     *           'province_user':'16',
     *           'city_user':'Almonacid Del Marquesado',
     *           'level_user':'bajo',
     *           'type_user':'subscriber',
     *           'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'
     *         }
     *     }
     */
    public function twitter_return()
    {
        if ($_POST['user'] && isset($_POST['user']['screen_name'])) {
            $arrargument = array(
                'cols' => array(
                    'user',
                ),
                'pattern' => array(
                    $_POST['user']['id'],
                ),
            );

            set_error_handler('ErrorHandler');
            try {
                //throw new Exception("Error Processing Request", 1);
                $result1 = loadModel(MODEL_LOGIN, 'login_model', 'get_user', $arrargument);
            } catch (Exception $e) {
                ///TEMPLATE ERROR//////////////
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema con el servidor, intentalo más tarde.';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            if ($result1) {
                $callback = '/salas';
                $jsondata['success'] = true;
                $jsondata['redirect'] = $callback;
                $jsondata['user'] = $result1[0];
                echo json_encode($jsondata);
                exit;
            } else {
                $date = date('m/d/Y', time());
                $arrargument = array(
                    'cols' => array(
                        'user',
                        'pass_user',
                        'name_user',
                        'lastname_user',
                        'birthday_user',
                        'reg_user',
                        'email_user',
                        'status_user',
                        'avatar_user',
                        'country_user',
                        'province_user',
                        'city_user',
                        'level_user',
                        'type_user',
                        'token',
                    ),
                    'pattern' => array(
                        $_POST['user']['id'],
                        '',
                        $_POST['user']['name'],
                        '',
                        '',
                        $date,
                        '',
                        'active',
                        $_POST['user']['profile_image_url'],
                        'default_country',
                        'default_province',
                        'default_town',
                        'bajo',
                        'subscriber',
                        '',
                    ),
                );

                set_error_handler('ErrorHandler');
                try {
                    $arrValue = loadModel(MODEL_LOGIN, 'login_model', 'create_user', $arrargument);
                } catch (Exception $e) {
                    ///TEMPLATE ERROR//////////////
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = 'Hubo un problema con el servidor, intentalo más tarde.';
                    echo json_encode($error_DB);
                    exit;
                }
                restore_error_handler();

                if ($arrValue) {
                    $callback = '/salas';
                    $jsondata['success'] = true;
                    $jsondata['redirect'] = $callback;
                    $jsondata['user'] = array(
                      'type' => 'alta',
                      'token' => $token,
                      'user' => $_POST['user']['id'],
                      'pass_user' => '',
                      'name_user' => $_POST['user']['name'],
                      'birthday_user' => '',
                      'lastname_user' => '',
                      'reg_user' => $date,
                      'email_user' => '',
                      'status_user' => 'active',
                      'avatar_user' => $_POST['user']['profile_image_url'],
                      'country_user' => 'default_country',
                      'province_user' => 'default_province',
                      'city_user' => 'default_town',
                      'level_user' => 'bajo',
                      'type_user' => 'subscriber',
                      'token' => '',
                  );

                    echo json_encode($jsondata);
                    exit;
                } else {
                    ///TEMPLATE ERROR//////////////
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = 'Hubo un problema con el servidor, intentalo más tarde.';
                    echo json_encode($error_DB);
                    exit;
                }
            }
        } else {
            ///TEMPLATE ERROR//////////////
            $error_DB['error_type'] = 500;
            $error_DB['error_message'] = 'Hubo un problema con el servidor, intentalo más tarde.';
            echo json_encode($error_DB);
            exit;
        }
    }
}
