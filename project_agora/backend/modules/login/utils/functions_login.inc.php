<?php

function validate_user($value)
{
    $error = array();
    $valido = true;
    $filtro = array(

        'user' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^.{2,20}$/'),
        ),
        'pass' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9a-zA-Z]{6,32}$/'),
        ),
        'conf_pass' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9a-zA-Z]{6,32}$/'),
        ),
        'email' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'validatemail',
        ),

    );

    $resultado = filter_var_array($value, $filtro);

    //no filter
    $resultado['level'] = $value['level'];

    if ($resultado['level'] === 'Seleccione nivel') {
        $error['level'] = "No ha seleccionado un nivel de inglés";
        $valido = false;
    }

    if ($resultado != null && $resultado) {
        if (!$resultado['user']) {
            $error['user'] = 'El user debe tener de 4 a 20 caracteres';
            $resultado['user'] = $value['user'];
            $valido = false;
        }
        if ((!$resultado['pass'] || $resultado['pass'] != $value['conf_pass'])) {
            $error['pass'] = 'El pass debe tener de 6 a 32 caracteres y las dos contrasenyas deben ser iguales';
            $resultado['pass'] = $value['pass'];
            $valido = false;
        }
        if ((!$resultado['conf_pass'] || $resultado['conf_pass'] != $value['pass'])) {
            $error['conf_pass'] = 'El pass debe tener de 6 a 32 caracteres y las dos contrasenyas deben ser iguales';
            $resultado['conf_pass'] = $value['conf_pass'];
            $valido = false;
        }

        if (!$resultado['email']) {
            $error['email'] = 'El email debe contener de 5 a 50 caracteres y debe ser un email válido';
            $resultado['email'] = $value['email'];
            $valido = false;
        }



    } else {
        $valido = false;
    }

    return $return = array('resultado' => $valido, 'error' => $error, 'datos' => $resultado);
}

function validatemail($email)
{
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if (filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^.{5,50}$/')))) {
            return $email;
        }
    }

    return false;
}

function get_gravatar( $email, $s = 80, $d = 'wavatar', $r = 'g', $img = false, $atts = array() ){
        $email = trim($email);
        $email = strtolower($email);
        $email_hash = md5($email);

        $url = "http://www.gravatar.com/avatar/".$email_hash;
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }
