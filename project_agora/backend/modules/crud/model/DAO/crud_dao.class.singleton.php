<?php

class crud_dao
{
    public static $_instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /* Get data in DB
        --> @params $db, $arrArgument --> array patterns
                                      --> array cols
                                      --> name table
        --> @return user
    */
    public function get_DAO($db, $arrargument)
    {
        $table = $arrargument['table'];
        $sql = 'SELECT * FROM '.$table.' WHERE ';

        for ($i = 0; $i < count($arrargument['cols']);++$i) {
            $sql .= $arrargument['cols'][$i]." = '".$arrargument['pattern'][$i]."' ";

            if ($i < (count($arrargument['cols']) - 1)) {
                $sql .= 'AND ';
            }
        }

        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    /* Delete data in DB
        --> @params $db, $arrArgument --> array patterns
                                      --> array cols
                                      --> name table
        --> @return user
    */
    public function delete_DAO($db, $arrargument)
    {
        $table = $arrargument['table'];
        $sql = 'DELETE FROM '.$table.' WHERE ';

        for ($i = 0; $i < count($arrargument['col']);++$i) {
            $sql .= $arrargument['col'][$i]." = '".$arrargument['pattern'][$i]."' ";

            if ($i < (count($arrargument['col']) - 1)) {
                $sql .= 'AND ';
            }
        }

        return $db->ejecutar($sql);
    }

    /* Create data
        --> @params $db, $arrArgument --> array patterns
                                      --> array cols
                                      --> name table
        --> @return true/false
    */
    public function create_DAO($db, $arrArgument)
    {
        $cols = '';
        $pattern = '';
        $table = $arrArgument['table'];

        for ($i = 0; $i < count($arrArgument['cols']);++$i) {
            $cols .= $arrArgument['cols'][$i];
            $pattern .= "'".$arrArgument['pattern'][$i]."'";

            if ($i < (count($arrArgument['cols']) - 1)) {
                $cols .= ', ';
                $pattern .= ', ';
            }
        }

        $sql = 'INSERT INTO '.$table.' ('.$cols.') VALUES ('.$pattern.')';

        return $db->ejecutar($sql);
    }

    /*
      General function to count in DB
            --> @params $db, $arrArgument --> pattern
                                          --> col--> name of column where you want check
                                          --> name table
            --> @return number
    */
    public function get_all_DAO($db, $arrArgument)
    {
        $table = $arrArgument['table'];
        $sql = 'SELECT * FROM '.$table;

        if (!empty($arrArgument['col'])) {
            $sql .= ' WHERE ';
            for ($i = 0; $i < count($arrArgument['col']);++$i) {
                $sql .= $arrArgument['col'][$i]." = '".$arrArgument['pattern'][$i]."' ";

                if ($i < (count($arrArgument['col']) - 1)) {
                    $sql .= 'AND ';
                }
            }
        }

        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    /*
      Upadate Users
            --> @params $db, $arrArgument --> columnData_Set
                                          --> columnData_Where
                                          --> name table
            --> @return true/false
    */
    public function update_DAO($db, $arrArgument)
    {
        $columnData_Set = $arrArgument['columnData_Set'];
        $columnData_Where = $arrArgument['columnData_Where'];
        $table = $arrArgument['table'];

        $sql = 'UPDATE '.$table.' SET '.$columnData_Set.' WHERE '.$columnData_Where;

        return $db->ejecutar($sql);
    }
}
