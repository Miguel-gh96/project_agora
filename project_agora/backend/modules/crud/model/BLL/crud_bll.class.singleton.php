<?php
class crud_bll {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = crud_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function get_all_BLL($arrargument) {
        return $this->dao->get_all_DAO($this->db, $arrargument);
    }

    public function get_BLL($arrargument) {
        return $this->dao->get_DAO($this->db, $arrargument);
    }

    public function create_BLL($arrArgument) {
        return $this->dao->create_DAO($this->db, $arrArgument);
    }

    public function update_BLL($arrArgument) {
        return $this->dao->update_DAO($this->db, $arrArgument);
    }

    public function delete_BLL($arrArgument) {
        return $this->dao->delete_DAO($this->db, $arrArgument);
    }


}
