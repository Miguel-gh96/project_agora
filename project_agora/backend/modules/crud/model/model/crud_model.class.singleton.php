<?php
class crud_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = crud_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }


    public function get_all($arrargument) {
        return $this->bll->get_all_BLL($arrargument);
    }

    public function get($arrargument) {
        return $this->bll->get_BLL($arrargument);
    }

    public function create($arrArgument) {
        return $this->bll->create_BLL($arrArgument);
    }

    public function update($arrArgument) {
        return $this->bll->update_BLL($arrArgument);
    }

    public function delete($arrArgument) {
        return $this->bll->delete_BLL($arrArgument);
    }



}
