<?php

class controller_crud {

    public function __construct() {
        include FUNCTIONS_CRUD . 'functions_crud.inc.php';
        $_SESSION['module'] = 'crud';
    }

    /**
     * @api {put} backend/index.php?module=crud&function=nuevo_usuario create_user
     * @apiName create_user
     * @apiGroup crud
     * @apiParam {json} data_user
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": true
     *     }
     */
    public function create_user() {
        if ((isset($_POST['data']))) {
            $jsondata = array();
            //$usersJSON = json_decode($_POST[0], true);
            $result = validate_user($_POST['data']);

            ///////////////CHECK if exist email/////////////////////////////////////
            $conf_email = array(
                'cols' => array('email_user'),
                'pattern' => array($result['datos']['email']), //
                'table' => 'users',
            );

            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_CRUD, 'crud_model', 'get', $conf_email);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            //if email exist in DB
            if (!empty($exist[0])) {
                $result['resultado'] = false;
                $result['error']['email'] = 'El email introducido ya existe.';
                ///////////////////////////
                echo json_encode($result);
                exit;
            }

            ///////////////CHECK if exist user/////////////////////////////////////
            $conf_user = array(
                'cols' => array('user'),
                'pattern' => array($result['datos']['user']),
                'table' => 'users',
            );

            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_CRUD, 'crud_model', 'get', $conf_user);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            //if user exist in DB
            if (!empty($exist[0])) {
                $result['resultado'] = false;
                $result['error']['user'] = 'El usuario introducido ya existe.';
            }

            if (($result['resultado'])) {
                //Obtain date sign up
                if (empty($result['datos']['reg_user'])) {
                    $result['datos']['reg_user'] = date('m/d/Y', time());
                }


                //Obtain token to active user
                if (empty($result['datos']['token'])) {
                    $result['datos']['token'] = md5(uniqid(microtime(), true));
                }

                //$grav_url1 = get_gravatar($result['datos']['email'], $s = 400, $d = 'identicon', $r = 'g', $img = false, $atts = array());
                /*
                  status:
                  inactive
                  active

                  type:
                  user
                  user_associated
                  admin
                 */

                $arrArgument = array(
                    'cols' => array(
                        'user',
                        'pass_user',
                        'name_user',
                        'lastname_user',
                        'birthday_user',
                        'reg_user',
                        'email_user',
                        'status_user',
                        'avatar_user',
                        'country_user',
                        'province_user',
                        'city_user',
                        'level_user',
                        'type_user',
                        'token',
                    ),
                    'pattern' => array(
                        $result['datos']['user'],
                        password_hash($result['datos']['pass'], PASSWORD_BCRYPT),
                        $result['datos']['name'],
                        $result['datos']['last_name'],
                        $result['datos']['birth_date'],
                        $result['datos']['reg_user'],
                        $result['datos']['email'],
                        $result['datos']['status'],
                        $result['datos']['avatar'],
                        $result['datos']['country'],
                        $result['datos']['province'],
                        $result['datos']['town'],
                        $result['datos']['en_lvl'],
                        $result['datos']['type_user'],
                        $result['datos']['token'],
                    ),
                    'table' => 'users',
                );

                /////////////////insert into BD////////////////////////
                $arrValue = false;

                set_error_handler('ErrorHandler');

                try {
                    $arrValue = loadModel(MODEL_CRUD, 'crud_model', 'create', $arrArgument);
                } catch (Exception $e) {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = '1Hubo un problema en la Base de Datos, intentalo más tarde';
                    echo json_encode($error_DB);
                    exit;
                }
                restore_error_handler();

                if ($arrValue) {
                    $jsondata['success'] = true;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = '2Hubo un problema en la Base de Datos, intentalo más tarde';
                    echo json_encode($error_DB);
                    exit;
                }
            } else {
                $jsondata['success'] = false;
                $jsondata['error'] = $result['error'];
                //header('HTTP/1.0 400 Bad error', true, 404);
                echo json_encode($jsondata);
                die();
            }
        }
    }

    /**
     * @api {post} backend/index.php?module=crud&function=actualizar_usuario update_user
     * @apiName update_user
     * @apiGroup crud
     *
     * @apiParam {Number} id Users unique ID.
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "firstname": "John",
     *       "lastname": "Doe"
     *     }
     */
    public function update_user() {
        if ((isset($_POST['data']))) {
            $jsondata = array();
            //$usersJSON = json_decode($_POST[0], true);
            $result = validate_user($_POST['data']);

            if (empty($_SESSION['result_avatar'])) {
                $_SESSION['result_avatar'] = array('resultado' => true, 'error' => '', 'datos' => $result['datos']['avatar']);
            }
            $result_avatar = $_SESSION['result_avatar'];

            ///////////////CHECK if exist email/////////////////////////////////////
            $conf_email = array(
                'cols' => array('email_user'),
                'pattern' => array($result['datos']['email']), //
                'table' => 'users',
            );

            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_CRUD, 'crud_model', 'get', $conf_email);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            //if email exist in DB
            if (!empty($exist[0])) {
                if ($exist[0]['email_user'] != $result['datos']['old_email']) {
                    $result['resultado'] = false;
                    $result['error']['email'] = 'El email introducido ya existe.';
                    ///////////////////////////
                    echo json_encode($result);
                    exit;
                }
            }

            ///////////////CHECK if exist user/////////////////////////////////////
            $conf_user = array(
                'cols' => array('user'),
                'pattern' => array($result['datos']['user']),
                'table' => 'users',
            );

            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_CRUD, 'crud_model', 'get', $conf_user);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            //if user exist in DB
            if (!empty($exist[0])) {
                if ($exist[0]['user'] != $result['datos']['old_user']) {
                    $result['resultado'] = false;
                    $result['error']['user'] = 'El usuario introducido ya existe.';
                }
            }

            ///check pass /////////////////////////
            if (strlen($result['datos']['pass']) >= 32) {
                $pass = $result['datos']['pass'];
            } else {
                $pass = password_hash($result['datos']['pass'], PASSWORD_BCRYPT);
            }

            if (($result['resultado'])) {
                //Obtain date sign up
                $date = date('m/d/Y', time());

                //Obtain token to active user
                $token = md5(uniqid(microtime(), true));

                //$grav_url1 = get_gravatar($result['datos']['email'], $s = 400, $d = 'identicon', $r = 'g', $img = false, $atts = array());
                /*
                  status:
                  inactive
                  active

                  type:
                  user
                  user_associated
                  admin
                 */
                $arrArgument = array(
                    'columnData_Set' => "user = '" . $result['datos']['user'] . "' ,"
                    . " pass_user = '" . $pass . "' ,"
                    . " name_user = '" . $result['datos']['name'] . "' ,"
                    . " lastname_user = '" . $result['datos']['last_name'] . "', "
                    . " email_user = '" . $result['datos']['email'] . "', "
                    . " birthday_user = '" . $result['datos']['birth_date'] . "', "
                    . " reg_user = '" . $result['datos']['reg_user'] . "', "
                    . " status_user = '" . $result['datos']['status'] . "', "
                    . " level_user = '" . $result['datos']['en_lvl'] . "', "
                    . " country_user = '" . $result['datos']['country'] . "', "
                    . " province_user = '" . $result['datos']['province'] . "', "
                    . " city_user = '" . $result['datos']['town'] . "', "
                    . " avatar_user = '" . $result_avatar['datos'] . "', "
                    . " token = '" . $result['datos']['token'] . "', "
                    . " type_user = '" . $result['datos']['type_user'] . "' ",
                    'columnData_Where' => " user = '" . $result['datos']['old_user'] . "'",
                    'table' => 'users',
                );

                /////////////////insert into BD////////////////////////
                $arrValue = false;

                set_error_handler('ErrorHandler');

                try {
                    $arrValue = loadModel(MODEL_CRUD, 'crud_model', 'update', $arrArgument);
                } catch (Exception $e) {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = '1Hubo un problema en la Base de Datos, intentalo más tarde';
                    echo json_encode($error_DB);
                    exit;
                }
                restore_error_handler();

                if ($arrValue) {
                    $jsondata['success'] = true;
                    $jsondata['user'] = array(
                        'user' => $result['datos']['user'],
                        'name_user' => $result['datos']['name'],
                        'lastname_user' => $result['datos']['last_name'],
                        'email_user' => $result['datos']['email'],
                        'birthday_user' => $result['datos']['birth_date'],
                        'level_user' => $result['datos']['en_lvl'],
                        'country_user' => $result['datos']['country'],
                        'province_user' => $result['datos']['province'],
                        'city_user' => $result['datos']['town'],
                        'avatar_user' => $result_avatar['datos'],
                        'status_user' => $result['datos']['status'],
                        'type_user' => $result['datos']['type_user'],
                        'token' => $result['datos']['token'],
                        'reg_user' => $result['datos']['reg_user'],
                        'pass_user' => $pass,
                    );
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = '2Hubo un problema en la Base de Datos, intentalo más tarde';
                    echo json_encode($error_DB);
                    exit;
                }
            } else {
                $jsondata['success'] = false;
                $jsondata['error'] = $result['error'];
                //header('HTTP/1.0 400 Bad error', true, 404);
                echo json_encode($jsondata);
                exit;
            }
        }
    }

    /**
     * @api {get} backend/index.php?module=crud&function=obtener_usuarios get_all_user
     * @apiName get_all_user
     * @apiGroup crud
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *
     *      "success" = true,
     *      "user" = {
     *          "user": "user",
     *          "pass_user": "pass_user",
     *          "name_user": "name_user",
     *          "lastname_user": "lastname_user",
     *          "birthday_user": "birthday_user",
     *          "reg_user": "reg_user",
     *          "email_user": "email_user",
     *          "status_user": "status_user",
     *          "avatar_user": "avatar_user",
     *          "country_user": "country_user",
     *          "province_user": "province_user",
     *          "city_user": "city_user",
     *          "level_user": "level_user",
     *          "type_user": "type_user",
     *          "token": "token"
     *     }
     */
    public function get_all_user() {
        $arrayArgument = array(
            'pattern' => '',
            'col' => '',
            'table' => 'users',
        );

        set_error_handler('ErrorHandler');
        try {
            $exist = loadModel(MODEL_CRUD, 'crud_model', 'get_all', $arrayArgument);
        } catch (Exception $e) {
            $error_DB['success'] = false;
            $error_DB['error_type'] = 500;
            $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
            echo json_encode($error_DB);
            exit;
        }
        restore_error_handler();

        if (!empty($exist)) {
            $jsondata['success'] = true;
            $jsondata['list_users'] = $exist;
            echo json_encode($jsondata);
            exit;
        } else {
            $error_DB['success'] = false;
            $error_DB['error_type'] = 500;
            $error_DB['error_message'] = '2Hubo un problema en la Base de Datos, intentalo más tarde';
            echo json_encode($error_DB);
            exit;
        }
    }

    /**
     * @api {get} backend/index.php?module=crud&function=obtener_usuarios get_user
     * @apiName get_user
     * @apiGroup crud
     * @apiParam {json} id
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *
     *      "success" = true,
     *      "user" = {
     *          "user": "user",
     *          "pass_user": "pass_user",
     *          "name_user": "name_user",
     *          "lastname_user": "lastname_user",
     *          "birthday_user": "birthday_user",
     *          "reg_user": "reg_user",
     *          "email_user": "email_user",
     *          "status_user": "status_user",
     *          "avatar_user": "avatar_user",
     *          "country_user": "country_user",
     *          "province_user": "province_user",
     *          "city_user": "city_user",
     *          "level_user": "level_user",
     *          "type_user": "type_user",
     *          "token": "token"
     *     }
     */
    public function get_user() {

        if ($_GET['id']) {
            $arrayArgument = array(
                'pattern' => array($_GET['id']),
                'cols' => array('user'),
                'table' => 'users',
            );

            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_CRUD, 'crud_model', 'get', $arrayArgument);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            if (!empty($exist)) {
                $jsondata['success'] = true;
                $jsondata['user'] = $exist[0];
                echo json_encode($jsondata);
                exit;
            } else {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = '2Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
        }
    }


    /**
     * @api {delete} backend/index.php?module=crud&function=borrar_usuario delete_user
     * @apiName delete_user
     * @apiGroup crud
     * @apiParam {json} id
     * @apiSuccessExample {json} jsondata Success:
     *     HTTP/1.1 200 OK
     *
     *      "success" = true,
     */

    public function delete_user() {
        if ($_GET['id']) {
            $arrayArgument = array(
                'pattern' => array($_GET['id']),
                'col' => array('user'),
                'table' => 'users',
            );

            set_error_handler('ErrorHandler');
            try {

                $exist = loadModel(MODEL_CRUD, 'crud_model', 'delete', $arrayArgument);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            if ($exist) {
                $jsondata['success'] = true;
                echo json_encode($jsondata);
                exit;
            } else {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
        }
    }

    /**
     * @api {get} backend/index.php?module=crud&function=obtener_salas get_all_rooms
     * @apiName get_all_rooms
     * @apiGroup crud
     * @apiSuccessExample {json} jsondata Success-Response:
     *     HTTP/1.1 200 OK
     *
     *      "success" = true,
     *      "list_rooms" = {
     *          "id_room": "id_room",
     *          "name_room": "name_room",
     *          "topic_room": "topic_room",
     *          "language_room": "language_room",
     *          "num_person_room": "num_person_room",
     *          "creation_date_room": "creation_date_room",
     *          "expire_date_room": "expire_date_room",
     *          "avatar_room": "avatar_room"
     *     }
     */

    public function get_all_rooms() {

        $arrayArgument = array(
            'pattern' => "",
            'col' => "",
            'table' => 'room',
        );

        set_error_handler('ErrorHandler');
        try {
            $exist = loadModel(MODEL_CRUD, 'crud_model', 'get_all', $arrayArgument);
        } catch (Exception $e) {
            $error_DB['success'] = false;
            $error_DB['error_type'] = 500;
            $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
            echo json_encode($error_DB);
            exit;
        }
        restore_error_handler();

        if (!empty($exist)) {
            $jsondata['success'] = true;
            $jsondata['list_rooms'] = $exist;
            echo json_encode($jsondata);
            exit;
        } else {
            $error_DB['success'] = false;
            $error_DB['error_type'] = 500;
            $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
            echo json_encode($error_DB);
            exit;
        }
    }


    /**
     * @api {delete} backend/index.php?module=crud&function=borrar_sala delete_room
     * @apiName delete_room
     * @apiGroup crud
     * @apiParam {json} id
     * @apiSuccessExample {json} jsondata Success:
     *     HTTP/1.1 200 OK
     *
     *      "success" = true,
     */
    public function delete_room() {

        if ($_GET['id']) {
            $arrayArgument = array(
                'pattern' => array($_GET['id']),
                'col' => array("id_room"),
                'table' => 'room',
            );

            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_CRUD, 'crud_model', 'delete', $arrayArgument);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            if ($exist) {
                $jsondata['success'] = true;
                echo json_encode($jsondata);
                exit;
            } else {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
        }
    }

}
