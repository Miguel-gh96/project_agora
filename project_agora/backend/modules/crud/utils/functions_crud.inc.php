<?php

function validate_user($value)
{
    $error = array();
    $valido = true;
    $filtro = array(
        'name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[A-Za-z-á-é-í-ó-ú\s]{2,30}$/'),
        ),
        'last_name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[A-Za-z-á-é-í-ó-ú\s]{2,30}$/'),
        ),
        'birth_date' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/'),
        ),
        'email' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'valida_email',
        ),
        'country' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-zA-Z_]*$/'),
        ),
        'province' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-zA-Z0-9, _]*$/'),
        ),
        'town' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'validate_town',
        ),
        'reg_user' => array(
          'filter' => FILTER_VALIDATE_REGEXP,
          'options' => array('regexp' => '/^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/'),
        ),
        'user' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^.{2,20}$/'),
        ),

    );

    $resultado = filter_var_array($value, $filtro);


    //no filter
    $resultado['en_lvl'] = $value['en_lvl'];
    $resultado['token'] = $value['token'];
    $resultado['old_email'] = $value['old_email'];
    $resultado['old_user'] = $value['old_user'];
    $resultado['avatar'] = $value['avatar'];
    $resultado['status'] = $value['status'];
    $resultado['type_user'] = $value['type_user'];
    $resultado['pass'] = $value['pass'];
    //end nofilter

    if ($resultado != '' && $resultado) {
        if ($value['name'] != '') {
            if (!$resultado['name']) {
                $error['name'] = 'Name must be 2 to 30 letters';
                $valido = false;
            }
        }

        if ($value['last_name'] != '') {
            if (!$resultado['last_name']) {
                $error['last_name'] = 'Last name must be 2 to 30 letters';
                $valido = false;
            }
        }

        if (!$resultado['email']) {
            $error['email'] = 'error format email (example@example.com)';
            $valido = false;
        }

        if ($value['birth_date'] != '') {
            if ($resultado['birth_date']) {
                //validate to user's over 16
              $dates = validateAge($resultado['birth_date']);

                if (!$dates) {
                    $error['birth_date'] = 'Debes ser mayor de 16.';
                    $valido = false;
                }
            }

            if (!$resultado['birth_date']) {
                $error['birth_date'] = 'error format date (mm/dd/yyyy)';
                $valido = false;
            }
        }

        if ($value['reg_user'] != '') {
            if (!$resultado['reg_user']) {
                $error['reg_user'] = 'error format date (mm/dd/yyyy)';
                $valido = false;
            }
        }

        if ($value['user'] != '') {
            if (!$resultado['user']) {
                $error['user'] = 'La contraseña debe contener entr 6 y 32 caracteres';
                $valido = false;
            }
        }


        if (!$resultado['country']) {
            $error['country'] = 'Seleccione un país correcto';
            $resultado['country'] = $value['country'];
            $valido = false;
        }

        if (!$resultado['province']) {
            $error['province'] = 'Seleccione una provincia correcta';
            $resultado['province'] = $value['province'];
            $valido = false;
        }

        if (!$resultado['town']) {
            $error['town'] = 'Seleccione una ciudad correcta';
            $resultado['town'] = $value['town'];
            $valido = false;
        }
    } else {
        $valido = false;
    };

    return $return = array('resultado' => $valido, 'error' => $error, 'datos' => $resultado);
}

function valida_dates($start_days, $dayslight)
{
    $start_day = date('m/d/Y', strtotime($start_days));
    $daylight = date('m/d/Y', strtotime($dayslight));

    list($mes_one, $dia_one, $anio_one) = split('/', $start_day);
    list($mes_two, $dia_two, $anio_two) = split('/', $daylight);

    $dateOne = new DateTime($anio_one.'-'.$mes_one.'-'.$dia_one);
    $dateTwo = new DateTime($anio_two.'-'.$mes_two.'-'.$dia_two);

    if ($dateOne <= $dateTwo) {
        return true;
    }

    return false;
}

// validate birthday
function validateAge($birthday, $age = 16)
{
    // $birthday can be UNIX_TIMESTAMP or just a string-date.
    if (is_string($birthday)) {
        $birthday = strtotime($birthday);
    }

    // check
    // 31536000 is the number of seconds in a 365 days year.
    if (time() - $birthday < $age * 31536000) {
        return false;
    }

    return true;
}

//validate email
function valida_email($email)
{
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if (filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^.{5,50}$/')))) {
            return $email;
        }
    }

    return false;
}

function validate_town($town)
{
    $town = filter_var($town, FILTER_SANITIZE_STRING);

    return $town;
}
