<?php

class rooms_bll {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = rooms_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function recoger_datos_BLL($arrargument) {

        return $this->dao->recoger_datos_DAO($this->db, $arrargument);
    }

    public function count_rooms_BLL($criteria) {

        return $this->dao->count_rooms_DAO($this->db, $criteria);
    }

}
