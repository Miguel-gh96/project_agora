<?php

class rooms_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = rooms_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function recoger_datos($arrargument) {

        return $this->bll->recoger_datos_BLL($arrargument);
    }

    public function count_rooms($criteria) {

        return $this->bll->count_rooms_BLL($criteria);
    }

}
