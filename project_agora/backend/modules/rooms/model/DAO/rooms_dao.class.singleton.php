<?php

class rooms_dao {

    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function recoger_datos_DAO($db, $arrargument) {
        $start = $arrargument["start"];
        $records_per_page = $arrargument["records_per_page"];
        $criteria = $arrargument["criteria"];

        $sql = "SELECT * from room WHERE topic_room like '%" . $criteria . "%' LIMIT $start, $records_per_page";


        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function count_rooms_DAO($db, $criteria) {

        $sql = "SELECT COUNT(*) as total FROM room WHERE topic_room like '%" . $criteria . "%'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

}
