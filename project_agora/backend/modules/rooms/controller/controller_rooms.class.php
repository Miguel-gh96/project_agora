<?php

class controller_rooms {

    public function __construct() {
        $_SESSION['module'] = "rooms";
    }

    public function checksum() {
        if (isset($_POST['cadena'])) {

            $checksum = sha1($_POST['cadena']);


            echo json_encode($checksum);
            exit;
        }
    }

    /**
     * @api {post} backend/index.php?module=rooms&function=recoger_datos recoger_datos
     * @apiName recoger_datos
     * @apiGroup rooms
     * @apiParam {json} data content p(numPages) busqueda
     * @apiSuccessExample {json} jsondata Response:
     *     HTTP/1.1 200 OK
     *    {
     *      "josndata" = {
     *          "id_room": "id_room",
     *          "name_room": "name_room",
     *          "topic_room": "topic_room",
     *          "language_room": "language_room",
     *          "num_person_room": "num_person_room",
     *          "creation_date_room": "creation_date_room",
     *          "expire_date_room": "expire_date_room",
     *          "avatar_room": "avatar_room"
     *       }
     *     }
     */
    public function recoger_datos() {
        if (isset($_POST['p']) && isset($_POST['busqueda'])) {
            $page = intval($_POST['p']);
            $busqueda = filter_string($_POST["busqueda"]);
            $criteria = $busqueda['datos'];

            if ($criteria === undefined) {
                $criteria = "";
            }

            $current_page = $page - 1;
            $records_per_page = 3; // records to show per page
            $start = $current_page * $records_per_page;


            set_error_handler('ErrorHandler');
            try {
                $resultado = loadModel(MODEL_ROOMS, "rooms_model", "count_rooms", $criteria);

                $total_rows = $resultado;

                $count = $total_rows[0]["total"];

                $total_pages = ceil($count / $records_per_page);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();

            if ($total_pages) {

                $arrargument = array(
                    'start' => $start,
                    'records_per_page' => $records_per_page,
                    'criteria' => $criteria
                );
                /* echo json_encode($page);
                  exit(); */
                if ($page > $total_pages) {
                    $josndata['end'] = true;
                    echo json_encode($josndata);
                    exit;
                }

                set_error_handler('ErrorHandler');
                try {
                    $result = loadModel(MODEL_ROOMS, "rooms_model", "recoger_datos", $arrargument);
                } catch (Exception $e) {

                    $error_DB['success'] = false;
                    $error_DB['error_type'] = 500;
                    $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                    echo json_encode($error_DB);
                    exit;
                }
                restore_error_handler();

                if ($result) {
                    echo json_encode($result);
                    exit;
                } else {
                    $error_DATA['success'] = false;
                    $error_DATA['error_type'] = 404;
                    $error_DATA['error_message'] = 'Su busqueda no obtuvo ninguna sala';
                    echo json_encode($error_DATA);
                    exit;
                }
            } else {
                $error_DATA['success'] = false;
                $error_DATA['error_type'] = 404;
                $error_DATA['error_message'] = 'Su busqueda no obtuvo ninguna sala';
                echo json_encode($error_DATA);
                exit;
            }
        }
    }

    /**
     * @api {get} backend/index.php?module=rooms&function=count_rooms count_rooms
     * @apiName count_rooms
     * @apiGroup rooms
     * @apiParam {json} id 
     * @apiSuccessExample {json} jsondata Response:
     *     HTTP/1.1 200 OK
     * {
     *      "$total_pages" : "total_pages"
     * }
     */
    public function count_rooms() {
        if (($_GET["id"])) {
            $result = filter_string($_GET["id"]);
            $criteria = $result['datos'];
            $records_per_page = 3;

            if ($criteria === undefined) {
                $criteria = "";
            }

            set_error_handler('ErrorHandler');
            try {
                $resultado = loadModel(MODEL_ROOMS, "rooms_model", "count_rooms", $criteria);

                $total_rows = $resultado;

                $count = $total_rows[0]["total"];

                $total_pages = ceil($count / $records_per_page);
            } catch (Exception $e) {
                $error_DB['success'] = false;
                $error_DB['error_type'] = 500;
                $error_DB['error_message'] = 'Hubo un problema en la Base de Datos, intentalo más tarde';
                echo json_encode($error_DB);
                exit;
            }
            restore_error_handler();
            echo json_encode($total_pages);
            exit;
            if ($total_pages) {

                echo json_encode($total_pages);
                exit;
            } else {
                $error_DATA['success'] = false;
                $error_DATA['error_type'] = 404;
                $error_DATA['error_message'] = 'Su busqueda no obtuvo ninguna sala';
                echo json_encode($error_DATA);
                exit;
            }
        }
    }

}
