
<?php

class controller_contact {

    public function __construct() {
        $_SESSION['module'] = 'contact';
    }

    /**
     * @api {post} backend/index.php?module=contact&function=process_contact process_contact
     * @apiName process_contact
     * @apiGroup Contact
     * @apiParam {post} token 
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *      success = 'Tu mensaje ha sido enviado'
     *     }
     */
    public function process_contact() {

        if ($_POST['token'] === 'contact_form') {

            //////////////// Envio del correo al usuario
            $arrArgument = array(
                'type' => 'contact',
                'token' => '',
                'inputName' => $_POST['inputName'],
                'inputEmail' => $_POST['inputEmail'],
                'inputSubject' => $_POST['inputSubject'],
                'inputMessage' => $_POST['inputMessage'],
            );
            set_error_handler('ErrorHandler');
            try {

                if (enviar_email($arrArgument)) {
                    echo "<div class='alert alert-success'>Tu mensaje ha sido enviado  </div>";
                } else {
                    echo "<div class='alert alert-error'>Server error. Intentalo más tarde...</div>";
                }
            } catch (Exception $e) {
                echo "<div class='alert alert-error'>Server error. Intentalo más tarde...</div>";
            }
            restore_error_handler();

            //////////////// Envio del correo al admin de la ap web
            $arrArgument = array(
                'type' => 'admin',
                'token' => '',
                'inputName' => $_POST['inputName'],
                'inputEmail' => $_POST['inputEmail'],
                'inputSubject' => $_POST['inputSubject'],
                'inputMessage' => $_POST['inputMessage'],
            );
            set_error_handler('ErrorHandler');
            try {
                if (enviar_email($arrArgument)) {
                    echo "<div class='alert alert-success'>Tu mensaje ha sido enviado </div>";
                } else {
                    echo "<div class='alert alert-error'>Server error. Intentalo más tarde...</div>";
                }
            } catch (Exception $e) {
                echo "<div class='alert alert-error'>Server error. Intentalo más tarde...</div>";
            }
            restore_error_handler();
        } else {
            echo "<div class='alert alert-error'>Server error. Intentalo más tarde...</div>";
        }
    }

}
