CREATE DATABASE  IF NOT EXISTS `agora` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `agora`;
-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: agora
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room` (
  `id_room` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name_room` varchar(45) DEFAULT NULL,
  `topic_room` varchar(200) DEFAULT NULL,
  `language_room` varchar(45) DEFAULT NULL,
  `num_person_room` int(11) DEFAULT NULL,
  `creation_date_room` varchar(45) DEFAULT NULL,
  `expire_date_room` varchar(45) DEFAULT NULL,
  `avatar_room` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id_room`),
  UNIQUE KEY `id_room_UNIQUE` (`id_room`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (00000000001,'Extreme','Minecraft','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000002,'Marionetas','Teatro','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000003,'Capitanes','Deportes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000004,'Girasoles','Informatica','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000005,'CTRL+V','Redes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000006,'Graneretes22','Mantenimiento','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000007,'UP','Gimnasio','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000008,'Promises','Programacion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000009,'Jaja','Comercio','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000010,'Numerets','Economia','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000011,'Benedicto666','Religion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000012,'MesiOle','Periodismo','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000013,'Pirulis','Artes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000014,'Come On','Estudios','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000015,'Beromers','Vivienda','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000016,'Nvidea','Videojuegos','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000017,'TarantinoRules','Peliculas','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000018,'FansRayden','Musica','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000019,'LQSA','Teatro','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000021,'Numerets2','Economia','english',0,'',NULL,'./backend/media/img_chat/chat.jpg'),(00000000022,'Jaja','Comercio','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000023,'Numerets','Economia','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000024,'Benedicto666','Religion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000025,'MesiOle','Periodismo','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000026,'Pirulis','Artes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000029,'Extreme','Minecraft','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000030,'Marionetas','Teatro','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000031,'Capitanes','Deportes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000032,'Girasoles','Informatica','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000033,'CTRL+V','Redes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000034,'Graneretes22','Mantenimiento','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000035,'UP','Gimnasio','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000036,'Promises','Programacion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000037,'Jaja','Comercio','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000038,'Numerets','Economia','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000039,'Benedicto666','Religion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000040,'MesiOle','Periodismo','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000041,'Pirulis','Artes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000042,'Come On','Estudios','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000043,'Beromers','Vivienda','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000044,'Nvidea','Videojuegos','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000045,'TarantinoRules','Peliculas','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000046,'FansRayden','Musica','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000047,'LQSA','Teatro','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000048,'Numerets2','Economia','english',0,'',NULL,'./backend/media/img_chat/chat.jpg'),(00000000049,'Jaja','Comercio','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000050,'Numerets','Economia','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000051,'Benedicto666','Religion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000052,'MesiOle','Periodismo','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000053,'Pirulis','Artes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000094,'Promises','Programacion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000095,'Jaja','Comercio','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000096,'Numerets','Economia','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000097,'Benedicto666','Religion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000098,'MesiOle','Periodismo','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000099,'Pirulis','Artes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000100,'Come On','Estudios','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000101,'Beromers','Vivienda','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000102,'Nvidea','Videojuegos','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000103,'TarantinoRules','Peliculas','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000104,'FansRayden','Musica','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000105,'LQSA','Teatro','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000106,'Numerets2','Economia','english',0,'',NULL,'./backend/media/img_chat/chat.jpg'),(00000000107,'Jaja','Comercio','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000108,'Numerets','Economia','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000109,'Benedicto666','Religion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000110,'MesiOle','Periodismo','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000111,'Pirulis','Artes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000112,'Extreme','Minecraft','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000113,'Marionetas','Teatro','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000114,'Capitanes','Deportes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000115,'Girasoles','Informatica','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000116,'CTRL+V','Redes','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000117,'Graneretes22','Mantenimiento','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000118,'UP','Gimnasio','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000119,'Promises','Programacion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000120,'Jaja','Comercio','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000121,'Numerets','Economia','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg'),(00000000122,'Benedicto666','Religion','english',0,NULL,NULL,'./backend/media/img_chat/chat.jpg');
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unsubscriber_user`
--

DROP TABLE IF EXISTS `unsubscriber_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unsubscriber_user` (
  `unsubscribe_date` datetime NOT NULL,
  `user_name` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`unsubscribe_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unsubscriber_user`
--

LOCK TABLES `unsubscriber_user` WRITE;
/*!40000 ALTER TABLE `unsubscriber_user` DISABLE KEYS */;
INSERT INTO `unsubscriber_user` VALUES ('2016-02-24 03:17:10','','cferrando.on@gmail.com'),('2016-02-24 03:22:16','12proves21aaa','project.agora.supp@gmsaail.comaaa');
/*!40000 ALTER TABLE `unsubscriber_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user` varchar(45) NOT NULL,
  `pass_user` varchar(100) DEFAULT NULL,
  `name_user` varchar(45) DEFAULT NULL,
  `lastname_user` varchar(60) DEFAULT NULL,
  `birthday_user` varchar(45) DEFAULT NULL,
  `reg_user` varchar(45) DEFAULT NULL,
  `email_user` varchar(80) DEFAULT NULL,
  `status_user` varchar(60) DEFAULT NULL,
  `avatar_user` varchar(200) DEFAULT NULL,
  `country_user` varchar(45) DEFAULT NULL,
  `province_user` varchar(45) DEFAULT NULL,
  `city_user` varchar(45) DEFAULT NULL,
  `level_user` varchar(45) DEFAULT NULL,
  `type_user` varchar(45) DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('12p4roves','$2y$10$g2OL2MmjtKnELoDRhXL2S.x0BVICfJJVDinuc6cO/yCsbUTzVDxZi','','','','02/24/2016','ct.agora.s4upp@gsaail.comaaa','active','./backend/media/default-avatar.png','default_country','default_province','default_town','bajo','subscriber','16e2e31faa02521397f92582214c300e'),('12proves','$2y$10$QrUY6jV9io1bsCvuX.2/luXwgT32b9UsdbmdLlTqIGi66hdh3dw5y','','','','02/24/2016','ct.agora.supp@gsaail.comaaa','active','./backend/media/default-avatar.png','default_country','default_province','default_town','bajo','subscriber','f03f776b431df1ad1439c9211d76782c'),('12proves2','$2y$10$0OY/LNWSSBuH61GTvBMJW.1ROg.lPXRgVIaUCVQT6DKsEbAdMv7Z2','','','','02/24/2016','ct.agora.supp@gmsaail.comaaa','active','./backend/media/default-avatar.png','default_country','default_province','default_town','bajo','subscriber','8d12c27153175f97eb7e4f0442e20f13'),('meganeo','$2y$10$vF0cFNqX0ZQ0EuRv1yX6LObTn.6VoAFQggEDfLBtU0hsHmFdS7tie','pepe','pepe','04/01/1987','01/01/2016','miky@gmail.com','active','./backend/media/default-avatar.png','ES','16','Almonacid Del Marquesado','medio','subscriber',NULL),('proves','$2y$10$vF0cFNqX0ZQ0EuRv1yX6LObTn.6VoAFQggEDfLBtU0hsHmFdS7tie','paquita','gandia','04/01/1987','01/01/2016','gandia@gmail.com','active','./backend/media/default-avatar.png','ES','16','Almonacid Del Marquesado','medio','admin','71b54545928cf37b84be674607bf49a9'),('proves21','$2y$10$GAxubfNP2VBmuMxyXiJhke/.cLCDW51KPZZXepvCoQYoDk3tsPUhu','','','','02/24/2016','project.agora.supp@gmail.com','active','./backend/media/default-avatar.png','default_country','default_province','default_town','bajo','subscriber','0c465d12778f597452de98fd6dc0c607'),('proves21a','$2y$10$YLAqAfqf1eX/uPN/bEUw9e1E2xDwWWOsFuzbZyjajNG7BjEU9BkTa','','','','02/24/2016','project.agora.supp@gmail.coma','active','./backend/media/default-avatar.png','default_country','default_province','default_town','bajo','subscriber','8a89bffd56f55555d8a0ba882e4b6480'),('proves21aa','$2y$10$2SyZZn6MZyJN3.R/F5pQE../AZazGpgcW3p0ndZhTgScyuk9fTcaS','','','','02/24/2016','project.agora.supp@gmail.comaa','active','./backend/media/default-avatar.png','default_country','default_province','default_town','bajo','subscriber','e403a0cc65ad90293a2619a22a820f5b'),('proves21aaa','$2y$10$S3bgLxisQapYC4l6wTVTGOyDbNfLcFYwjHwGNkg0NZsBfNyMUXBZG','','','','02/24/2016','project.agora.supp@gmail.comaaa','active','./backend/media/default-avatar.png','default_country','default_province','default_town','bajo','subscriber','d9f5a1af3ed216ed69ba38f7adc61785');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `agora`.`users_AFTER_DELETE` AFTER DELETE ON `users` FOR EACH ROW
BEGIN
INSERT INTO `agora`.`unsubscriber_user`(`unsubscribe_date`,`name`,`email`) VALUES(NOW(),old.user,old.email_user);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'agora'
--

--
-- Dumping routines for database 'agora'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-24  3:23:19
