app.factory("services", ['$http','$q', function ($http,$q) {
    var serviceBase = 'backend/index.php?module=';
    var obj = {};

        obj.get = function (module, functi) {
            var defered=$q.defer();
            var promise=defered.promise;
            $http({
                  method: 'GET',
                  url: serviceBase + module + '&function=' + functi
              }).success(function(data, status, headers, config) {
                 defered.resolve(data);
              }).error(function(data, status, headers, config) {
                 defered.reject(data);
              });
            return promise;
            //return $http.get(serviceBase + module + '&function=' + functi);
            //return $http.get('app_model/index.php?module=customer&function=customers');
        };

        obj.get = function (module, functi, dada) {
            var defered=$q.defer();
            var promise=defered.promise;
            $http({
                  method: 'GET',
                  url: serviceBase + module + '&function=' + functi + '&id=' + dada
              }).success(function(data, status, headers, config) {
                 //console.log(data);
                 defered.resolve(data);
              }).error(function(data, status, headers, config) {
                 defered.reject(data);
              });
            return promise;
            //return $http.get(serviceBase + module + '&function=' + functi + '&id=' + dada);
            //return $http.get('app_model/index.php?module=customer&function=customer&id='+customerID);
        };

        obj.post = function (module, functi, dada) {
          var defered=$q.defer();
          var promise=defered.promise;
          $http({
                method: 'POST',
                url: serviceBase + module + '&function=' + functi,
                data:dada
            }).success(function(data, status, headers, config) {
      	       defered.resolve(data);
            }).error(function(data, status, headers, config) {
               defered.reject(data);
            });
          return promise;
            //return $http.post(serviceBase + module + '&function=' + functi, dada);
            //return $http.post('app_model/index.php?module=customer&function=insertCustomer',customer);
        };

        obj.put = function (module, functi, dada) {
          var defered=$q.defer();
          var promise=defered.promise;
          $http({
                method: 'PUT',
                url: serviceBase + module + '&function=' + functi,
                data:dada
            }).success(function(data, status, headers, config) {
      	       defered.resolve(data);
            }).error(function(data, status, headers, config) {
               defered.reject(data);
            });
          return promise;
            //return $http.put(serviceBase + module + '&function=' + functi ,{'customer': dada});
            //return $http.post('app_model/index.php?module=customer&function=updateCustomer',{id:id, customer:customer});
        };

        obj.delete = function (module, functi, dada) {
            var defered=$q.defer();
            var promise=defered.promise;
            $http({
                  method: 'DELETE',
                  url: serviceBase + module + '&function=' + functi + '&id=' + dada
              }).success(function(data, status, headers, config) {
                 //console.log(data);
                 defered.resolve(data);
              }).error(function(data, status, headers, config) {
                 defered.reject(data);
              });
            return promise;
            //return $http.delete(serviceBase + module + '&function=' + functi + '&id=' + dada);
            //return $http.delete('app_model/index.php?module=customer&function=deleteCustomer&id='+id);
        };
        obj.getXML = function(routeExist) {
          var defered=$q.defer();
          var promise=defered.promise;
          $http({
                method: 'GET',
                url: routeExist
            }).success(function(data, status, headers, config) {
               defered.resolve(data);
            }).error(function(data, status, headers, config) {
               defered.reject(data);
            });
          return promise;
          //return $http.get(routeExist);
        }

    return obj;
}]);
