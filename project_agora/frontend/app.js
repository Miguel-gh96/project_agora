var app = angular.module('myApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'infinite-scroll', 'ngCookies', 'facebook', 'xml']);
//'ui.bootstrap' -> necesario para la paginación

app.config(['$routeProvider', '$httpProvider', 'x2jsProvider',
  function($routeProvider, $httpProvider, x2jsProvider) {
    $httpProvider.interceptors.push('xmlHttpInterceptor');
    $routeProvider

    // Home
      .when("/", {
      templateUrl: "frontend/modules/home/view/home.view.html",
      controller: "home",
      resolve: {
        dataU_service: function(services, $route) {
          var token = 0;
          return token;
        }
      }

    })

    // terms
    .when("/terminos", {
      templateUrl: "frontend/modules/terms/view/terms.view.html",
      controller: "terms"
    })

    // rooms
    .when("/salas", {
      title: 'Salas',
      templateUrl: "frontend/modules/rooms/view/rooms.view.html",
      controller: "rooms",
    })



    // Team
    .when("/equipo", {
      templateUrl: "frontend/modules/team/view/team.view.html",
      controller: "team"
    })

    // Contact
    .when("/contacto", {
      templateUrl: "frontend/modules/contact/view/contact.view.html",
      controller: "contact"
    })

    // activate user
    .when("/estado/:token", {
        templateUrl: "frontend/modules/home/view/home.view.html",
        controller: "home",
        resolve: {
          dataU_service: function(services, $route) {
            var token = $route.current.params.token;
            return services.get('login', 'activar', token);
          }
        }
      })
      // recover_pass2
      .when("/estado/recuperar/:token", {
        templateUrl: "frontend/modules/login/view/recover_pass2.view.html",
        controller: "recover_pass2",
        resolve: {
          dataU_service: function(services, $route) {
            var token = $route.current.params.token;
            return services.get('login', 'activar', token);
          }
        }
      })


    // Privacy
    .when("/privacidad", {
      templateUrl: "frontend/modules/privacy/view/privacy.view.html",
      controller: "privacy"
    })

    // Users
    .when("/perfil", {
      templateUrl: "frontend/modules/users/view/profile_personal.view.html",
      controller: "profile_personal"
    })

    .when("/configuracion", {
      templateUrl: "frontend/modules/users/view/profile_conf.view.html",
      controller: "profile_conf"
    })

    .when("/listar_usuarios", {
        templateUrl: "frontend/modules/crud/view/list_users.view.html",
        controller: "list_users"
      })
      .when("/listar_usuarios/nuevo_usuario", {
        templateUrl: "frontend/modules/crud/view/user.view.html",
        controller: "new_user"
      })
      .when("/listar_usuarios/editar_usuario/:id", { //:id
        templateUrl: "frontend/modules/crud/view/user.view.html",
        controller: "edit_user",
        resolve: {
          dataU_service: function(services, $route) {
            var id = $route.current.params.id;
            return services.get('crud', 'obtener_usuario', id); //
          }
        }
      })

    .when("/listar_salas", {
      templateUrl: "frontend/modules/crud/view/list_rooms.view.html",
      controller: "list_rooms"
    })

    .otherwise("/", {
      templateUrl: "frontend/modules/home/view/home.view.html",
      controller: "home"
    });

  }
]).run(function($rootScope, commonService, $location) {

  //list pages with restricted access
  var restricted_access = [
    'rooms',
    'profile_conf',
    'profile_personal',
    'list_users',

  ]

  //list pages that only admin access
  var only_admin = [
    'list_users',
    'new_user',
    'edit_user',
    'list_rooms'

  ]


  $('.navbar-nav').on('click', 'li a', function() {
    $('.navbar-collapse').collapse('hide');
  });

  $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {

    //Restricted access to pages list in restricted_access variable.
    for (var i = 0; i <= restricted_access.length; i++) {
      if (current.$$route === undefined) {
        $location.path('/');
      } else {
        if (restricted_access[i] === current.$$route.controller) {
          console.log(current.$$route.controller);
          if (commonService.get_userCookie() === false) {
            $location.path('/');
          }
        }

        //Only admin  access to pages list in only_admin variable.
        if (commonService.get_userCookie() === true) {
          for (var i = 0; i <= only_admin.length; i++) {
            if(current.$$route.controller === only_admin[i]){
              if($rootScope.globals.currentUser.type_user != 'admin'){
                $location.path('/');
              }
            }

          }
        }


        //we use this services to print menu with  slider or only menu
        commonService.print_menu(current.$$route.controller);
        //we use this services to print user logged or no.
        commonService.get_userCookie();

      }

    }

  });

});

app.config(['FacebookProvider',
  function(FacebookProvider) {
    var myAppId = '1698405647083590';

    FacebookProvider.init(myAppId);
  }
]);
