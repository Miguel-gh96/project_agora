app.factory("commonService", ['$rootScope','$cookies','$location', function ($rootScope,$cookies,$location) {
        var service = {};
        service.print_menu = print_menu;
        service.get_userCookie = get_userCookie;
        return service;


        function print_menu(home){
          if(home === 'home'){
            $rootScope.class_menu = 'main';
          }else{
            $rootScope.class_menu = 'past-main';
          }
        }

        function get_userCookie () {

          var user_cookie = $cookies.getObject('globals');

          if(user_cookie){
            $rootScope.globals = user_cookie;
            return true;
          }

          return false;
        }


}]);
