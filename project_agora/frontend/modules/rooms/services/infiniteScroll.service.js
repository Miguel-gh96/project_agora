//constructor function to encapsulate HTTP and pagination logic
angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 350);
app.factory('Scroll', function(services, $http) {
  var Scroll = function() {
    this.items = [];
    this.keyword = '';
    this.busy = false;
    this.after = 1;
    this.error = {};
  };

  Scroll.prototype.nextPage = function() {
      if (this.busy) return;
      this.busy = true;
      var this_ = this;

      services.post('rooms', 'recoger_datos', JSON.stringify({
          'p': this_.after,
          'busqueda': this_.keyword
        }))
        .then(function(data, status) {
          //console.log(data);
          if (data.error_type) {
            this_.items = "";
            this_.error = data;
          } else if (data.end) {

          } else {

            var items1 = data;

            for (var i = 0; i < items1.length; i++) {
              this_.items.push(items1[i]);
            }

            Scroll.after = this_.after++;
            this_.busy = false;

          }

        });


  };

  return Scroll;
});
