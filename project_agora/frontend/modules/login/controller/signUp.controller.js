app.controller('signUp', function($scope,FacebookService, twitterService, AuthenticationService,
  $uibModalInstance, $uibModal, $location, services, $sce, $location /*, , $http */ ) {
  console.log("signUp");
  twitterService.initialize();
  $scope.signUp_data = {
    data: {
      user: "",
      pass: "",
      conf_pass: "",
      email: "",
      level: "Seleccione nivel",
    },
    error: {
      error_DB: "",
      user: "",
      pass: "",
      conf_pass: "",
      email: "",
      level: ""
    }
  }

  $scope.validateForm = function() {
    if ((!$scope.form_signUp.$valid) || $scope.signUp_data.data.level === "Seleccione nivel") {
      console.log("bien");
      angular.forEach($scope.form_signUp.$error.required, function(field) {
        field.$setDirty();
      });
      if ($scope.signUp_data.data.level === "Seleccione nivel")
        $scope.signUp_data.error.level = $sce.trustAsHtml('Seleccione un nivel');

      return false;
    } else {
      $scope.signUp();
      return true;
    }
  }



  /////////////SigUp////////////////////////////
  $scope.signUp = function() {

    //Clean errors
    for (name in $scope.signUp_data.error) {
      $scope.signUp_data.error[name] = "";
    }

    var data = JSON.stringify({
      data: $scope.signUp_data.data
    });

    services.post('login', 'nuevo', data)
      .then(function(results) {
        console.log(results);
        if (results.success) {
          $scope.open('md', './frontend/modules/login/view/info_user_signUp.view.html', 'info_user');
        } else {
          console.log(results.error_message);
          $scope.signUp_data.error.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>" + results.error_message + "</span>");
        }
      }, function errorCallback(results) {
        //console.log(results);

        //server control errors
        for (name in $scope.signUp_data.error) {
          if (results.error[name]) {
            $scope.signUp_data.error[name] = $sce.trustAsHtml(results.error[name]);
          }
        }

      });

  };
  ////////////end SignUp ///////////////////////

  /////////////SigUpFace////////////////////////////

  $scope.facebook = function() {

    var conected = FacebookService.IntentLogin();
    if (conected === 'NoConnected') {
      var logged = FacebookService.login();
      if (logged = true) {
        $uibModalInstance.close();
      }
    }


  };

  ////////////end SignUpFace ///////////////////////


  ////////////MODAL/////////////////////////////
  $scope.ok = function() {
    $uibModalInstance.close();
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };

  $scope.animationsEnabled = true;

  $scope.open = function(size, url, controller) {
      $uibModalInstance.close();
      setTimeout(function() {
        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: url,
          controller: controller,
          size: size,
          resolve: {
            items: function() {
              return $scope.items;
            }
          }
        });
      }, 500);
    }
    ///////////////END MODAL/////////////////////////////


  /////////////twitter logIn//////////////////////////
  //using the OAuth authorization result get  user
  $scope.refreshTimeline = function() {
    twitterService.getUserTwitter().then(function(data) {
      services.post('login', 'twitter_return', JSON.stringify({
          'user': data
        }))
        .then(function(results) {
          //console.log(results);
          if (results.success) {
            //add use in cookies
            $uibModalInstance.close();
            AuthenticationService.SetCredentials(results.user);
            $location.path(results.redirect);
          } else {
            console.log(results.error_message);
            $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>" + results.error_message + "</span>");
          }

        });
    });
  }

  //when the user clicks the connect twitter button, the popup authorization window opens
  $scope.connectTwitter = function() {
      twitterService.connectTwitter().then(function() {
        if (twitterService.isReady()) {
          //if the authorization is successful, hide the connect button and display user
          $scope.refreshTimeline();

        }
      });
    }
    /////////////end twitter logIn////////////////////////


});
