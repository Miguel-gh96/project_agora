app.controller('recover_pass2', function($scope, dataU_service, $rootScope, $sce,services, $location,AuthenticationService/*, $location, $http */ ) {

if (dataU_service != 0 && dataU_service.error_DB === undefined) {
console.log(dataU_service);
  $scope.recover_data = {
    pass:"",
    conf_pass:""
  };

  $scope.recover_pass = function() {

    if ($scope.forget_pass.pass.$error.required != true) {
      if ($scope.forget_pass.pass.$error.pattern != true) {
        if($scope.recover_data.pass === $scope.recover_data.conf_pass){

          var data = {
            'id':$scope.recover_data.pass,
            'token':dataU_service.user.token
          }
          console.log(data);
          services.put('login', 'new_pass', data)
            .then(function(results) {
              console.log(results);
              if (results.success) {
                //update variable user
                dataU_service.user.pass_user = $scope.recover_data.pass;
                //create cookie
                AuthenticationService.SetCredentials(dataU_service.user);
                $location.path('/salas');
              } else {
                console.log(results.error_message);
                $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>" + results.error_message + "</span>");
              }
            });
        }else{
          $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>Las contraseñas no coinciden</span>");
        }
      } else {
        $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>El password debe ser mínimo de 6 carácteres</span>");
      };
    }else{
      $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>El campo es obligatorio</span>");
    }

  }

}else{
  $location.path("/");
}
});
