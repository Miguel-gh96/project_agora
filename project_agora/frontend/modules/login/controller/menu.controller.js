app.controller('menu', function(FacebookService, Facebook,$scope, $uibModal, AuthenticationService ) {

  $scope.close_session = function () {
      //Logout Facebook
      FacebookService.logout();
      AuthenticationService.ClearCredentials();
  };

  /////////////////////MODAL////////////////////////
  $scope.items = [];
  $scope.animationsEnabled = true;
  $scope.close_session = function() {
    AuthenticationService.ClearCredentials();
  }

  $scope.open = function(size, url, controller) {
    console.log(url + " controller: " + controller);
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: url,
      controller: controller,
      size: size,
      resolve: {
        items: function() {
          return $scope.items;
        }
      }
    });

  };
  /////////////////////end MODAL////////////////////////

});
