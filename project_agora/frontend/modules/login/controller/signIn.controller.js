app.controller('signIn', function(FacebookService,$scope, twitterService, $uibModalInstance, $uibModal, $location, services, $sce, AuthenticationService, $location) {
  twitterService.initialize();
  $scope.signIn_data = {
    user: "",
    pass: ""
  }
  $scope.error_DB = undefined;


  /////////////SigIn////////////////////////////
  $scope.signIn = function() {
    $scope.error_DB = "";

    if ($scope.signIn_data.user != undefined) {
      if ($scope.signIn_data.pass != undefined) {
        services.post('login', 'exist_user', JSON.stringify($scope.signIn_data))
          .then(function(results) {
            console.log(results);
            if (results.success) {
              //add use in cookies

              $uibModalInstance.close();
              AuthenticationService.SetCredentials(results.user);
              $location.path(results.redirect);
            } else {
              console.log(results.error_message);
              $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>" + results.error_message + "</span>");
            }

          });
      } else {
        $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>Se requiere contraseña</span>");
      }
    } else {
      $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>El usuario es obligatorio</span>");
    }

  };
  ////////////end SignIn ///////////////////////



  /////////////SigInFace////////////////////////////

  $scope.facebook = function() {

    var conected = FacebookService.IntentLogin();
    if (conected === 'NoConnected') {
      var logged = FacebookService.login();
      if (logged = true) {
        $uibModalInstance.close();
      }
    }


  };

  ////////////end SignInFace ///////////////////////



  ////////////MODAL/////////////////////////////
  $scope.ok = function() {
    $uibModalInstance.close();
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };

  $scope.animationsEnabled = true;

  $scope.open = function(size, url, controller) {
    $uibModalInstance.close();
    setTimeout(function() {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: url,
        controller: controller,
        size: size,
        resolve: {
          items: function() {
            return $scope.items;
          }
        }
      });
    }, 500);

  };
  ///////////////END MODAL/////////////////////////////


  /////////////twitter logIn//////////////////////////
  //using the OAuth authorization result get  user
  $scope.refreshTimeline = function() {
    twitterService.getUserTwitter().then(function(data) {
      services.post('login', 'twitter_return', JSON.stringify({
          'user': data
        }))
        .then(function(results) {
          console.log(results);
          if (results.success) {
            //add use in cookies
            $uibModalInstance.close();
            AuthenticationService.SetCredentials(results.user);
            $location.path(results.redirect);
          } else {
            console.log(results.error_message);
            $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>" + results.error_message + "</span>");
          }


        });
    });
  }

  //when the user clicks the connect twitter button, the popup authorization window opens
  $scope.connectTwitter = function() {
      twitterService.connectTwitter().then(function() {
        if (twitterService.isReady()) {
          //if the authorization is successful, hide the connect button and display user
          $scope.refreshTimeline();

        }
      });
    }
    /////////////end twitter logIn////////////////////////


});
