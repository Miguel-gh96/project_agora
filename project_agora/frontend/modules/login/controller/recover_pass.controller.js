app.controller('recover_pass', function($scope, $uibModalInstance, $uibModal,$sce,services/*, $location, $http */ ) {
  console.log("recover_pass");

  $scope.email = "";

  $scope.recover_pass = function() {
    console.log($scope.form_signIn.email.$error.pattern);
    if ($scope.form_signIn.email.$error.required != true) {
      if ($scope.form_signIn.email.$error.pattern != true) {
        services.get('login', 'exist_email', $scope.email)
          .then(function(results) {
            console.log(results);
            if (results.success) {
              $scope.open('md','./frontend/modules/login/view/info_user_recover.view.html','info_user');
            } else {
              console.log(results.error_message);
              $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>" + results.error_message + "</span>");
            }
          });
      } else {
        $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>Error formato email (ejemplo@ejemplo.com)</span>");
      };
    }else{
      $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>El campo es obligatorio</span>");
    }

  }


  ////////////MODAL/////////////////////////////
  $scope.ok = function() {
    $uibModalInstance.close();
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };

  $scope.animationsEnabled = true;

  $scope.open = function(size, url, controller) {
    $uibModalInstance.close();
    setTimeout(function() {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: url,
        controller: controller,
        size: size,
        resolve: {
          items: function() {
            return $scope.items;
          }
        }
      });
    }, 500);

  };
  ///////////////END MODAL/////////////////////////////




});
