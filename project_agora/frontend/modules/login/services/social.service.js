app.factory("FacebookService", ['$timeout', '$rootScope', '$q', 'Facebook', 'services', 'AuthenticationService','$location',
  function($timeout, $rootScope, $q, Facebook, services, AuthenticationService,$location) {
    var service = {};
    service.IntentLogin = IntentLogin;
    service.login = login;
    service.me = me;
    service.logout = logout;
    return service;


    // Define user empty data :/
    $rootScope.user = {};

    // Defining user logged status
    $rootScope.logged = false;

    // And some fancy flags to display messages upon user status change
    $rootScope.byebye = false;
    $rootScope.salutation = false;

    /**
     * Watch for Facebook to be ready.
     * There's also the event that could be used
     */
    $rootScope.$watch(
      function() {
        return Facebook.isReady();
      },
      function(newVal) {
        if (newVal)
          $rootScope.facebookReady = true;
      }
    );

    var userIsConnected = false;
    Facebook.getLoginStatus(function(response) {
      if (response.status == 'connected') {
        userIsConnected = true;
      }
    });

    function IntentLogin() {

      if (!userIsConnected) {
        return "NoConnected";
        //login();

      }
      return "Connected";
    }


    function login() {
      Facebook.login(function(response) {
        if (response.status == 'connected') {
          $rootScope.logged = true;
          me();
          return $rootScope.logged;
        }
      });

    }


    function me() {

      Facebook.api('me?fields=first_name,last_name,email,picture', function(response) {
        /**
         * Using $scope.$apply since this happens outside angular framework.
         */
        //console.log(response);
        var emuser = response.email.split("@");


        $rootScope.$apply(function() {
          data = {
            user: emuser[0],
            email: response.email,
            first_name: response.first_name,
            id: response.id

          }
          //console.log(data);

          var data2 = JSON.stringify({
            data: data
          });

          services.post('login', 'social_facebook', data2)
            .then(function(results) {

              //console.log(results);

              if (results.success) {

                $rootScope.user = {
                  user: results.user.user,
                  pass_user: results.user.pass_user,
                  name_user: results.user.name_user,
                  lastname_user: results.user.last_name,
                  birthday_user: results.user.birthday_user,
                  reg_user: results.user.reg_user,
                  email_user: results.user.email_user,
                  status_user: results.user.status_user,
                  avatar_user: results.user.avatar_user,
                  country_user: results.user.country_user,
                  province_user: results.user.province_user,
                  city_user: results.user.city_user,
                  level_user: results.user.level_user,
                  type_user: results.user.type_user,
                  token: results.user.token

                };
                AuthenticationService.SetCredentials($rootScope.user);
                $location.path('/salas');
              }else{
                $location.path('/');
              }
            });
        });
      });


    };

    function logout() {
      Facebook.logout(function() {
        $rootScope.$apply(function() {
          $rootScope.user = {};
          $rootScope.logged = false;
        });
      });
    };

    $rootScope.$on('Facebook:statusChange', function(ev, data) {
      //console.log('Status: ', data);
      if (data.status == 'connected') {
        $rootScope.$apply(function() {
          $rootScope.salutation = true;
          $rootScope.byebye = false;
        });
      } else {
        $rootScope.$apply(function() {
          $rootScope.salutation = false;
          $rootScope.byebye = true;

          // Dismiss byebye message after two seconds
          $timeout(function() {
            $rootScope.byebye = false;
          }, 2000);
        });
      }
    });

  }
]);



app.factory('twitterService', function($q,$location) {

  var authorizationResult = false;

  return {
    initialize: function() {
      //initialize OAuth.io with public key of the application
      OAuth.initialize('CeXWPGVMTeh7KWFyD7Elwdqw-MA', {
        cache: true
      });
      //try to create an authorization result when the page loads, this means a returning user won't have to click the twitter button again
      authorizationResult = OAuth.create('twitter');
    },
    isReady: function() {
      return (authorizationResult);
    },
    connectTwitter: function() {
      var deferred = $q.defer();
      OAuth.popup('twitter', {
        cache: true
      }, function(error, result) { //cache means to execute the callback if the tokens are already present
        if (!error) {
          authorizationResult = result;
          deferred.resolve();
        } else {
          //do something if there's an error
          $location.path('/');
        }
      });
      return deferred.promise;
    },
    clearCache: function() {
      OAuth.clearCache('twitter');
      authorizationResult = false;
    },
    getLatestTweets: function() {
      //create a deferred object using Angular's $q service
      var deferred = $q.defer();
      var promise = authorizationResult.get('/1.1/statuses/home_timeline.json').done(function(data) { //https://dev.twitter.com/docs/api/1.1/get/statuses/home_timeline
        //when the data is retrieved resolved the deferred object
        deferred.resolve(data)
      });
      //return the promise of the deferred object
      return deferred.promise;
    },
    getUserTwitter: function() {
      //create a deferred object using Angular's $q service
      var deferred = $q.defer();
      var promise = authorizationResult.get('/1.1/account/verify_credentials.json').done(function(data) { //https://dev.twitter.com/docs/api/1.1/get/statuses/home_timeline
        //when the data is retrieved resolved the deferred object
        deferred.resolve(data)
      });
      //return the promise of the deferred object
      return deferred.promise;
    }

  }

});
