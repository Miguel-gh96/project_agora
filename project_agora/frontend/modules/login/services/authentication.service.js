app.factory("AuthenticationService", ['$http', '$cookies', '$rootScope', '$timeout', 'UserService',
    function ($http, $cookies, $rootScope, $timeout, UserService) {
        var service = {};
        service.Login = Login;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
        return service;

        function Login(email, password, callback) {
            $timeout(function () {
                var response;
                UserService.GetByUsername(email)
                    .then(function (response) {
                        console.log(response.success);
                        console.log(response.message);
                        console.log(response.data);
                        callback(response);
                    });
            }, 1000);
        }

        function SetCredentials(user) {
            var authdata = Base64.encode(user.email_user + ':' + user.pass_user);
            $rootScope.globals = {
                currentUser: {
                  user:user.user,
                  pass_user:user.pass_user,
                  name_user:user.name_user,
                  lastname_user:user.lastname_user,
                  birthday_user:user.birthday_user,
                  reg_user:user.reg_user,
                  email_user:user.email_user,
                  status_user:user.status_user,
                  avatar_user:user.avatar_user,
                  country_user:user.country_user,
                  province_user:user.province_user,
                  city_user:user.city_user,
                  level_user:user.level_user,
                  type_user:user.type_user,
                  token:user.token
                }
            };
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
            //change to object
            //cookie persistent <<expires:(new date(new date.gettime() +24*60*60*1000))>>
            var d = new Date();
            var date = new Date(d.getTime() +24*60*60*1000);
            $cookies.putObject('globals', $rootScope.globals,{expires:date});
        }

        function ClearCredentials() {
            $rootScope.globals = undefined;
            $cookies.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic';
        }
}]);

    // Base64 encoding service used by AuthenticationService
    var Base64 = {
        keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
            return output;
        },
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = this.keyStr.indexOf(input.charAt(i++));
                enc2 = this.keyStr.indexOf(input.charAt(i++));
                enc3 = this.keyStr.indexOf(input.charAt(i++));
                enc4 = this.keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
            return output;
        }
    };
