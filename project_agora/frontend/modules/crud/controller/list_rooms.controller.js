app.controller('list_rooms', function($scope, services, $route) {

  /* RETRIEVE ROOMS LISTING FROM API */
  services.get('crud', 'obtener_salas')
    .then(function(response) {

      if (response.success) {
        console.log(response);
        $scope.rooms = response.list_rooms;
        $scope.viewby = 5;
        $scope.totalItems = $scope.rooms.length;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 5; //Number of pager buttons to show
      }else{
        $location.path('/');
      }

    });


  $scope.setPage = function(pageNo) {
    $scope.currentPage = pageNo;
  };

  $scope.pageChanged = function() {
    console.log('Page changed to: ' + $scope.currentPage);
  };

  $scope.setItemsPerPage = function(num) {
    $scope.itemsPerPage = num;
    $scope.currentPage = 1; //reset to first paghe
  }

  $scope.setRooms = function(id) {
    for (var i = 0; i < $scope.rooms.length; i++) {
      if ($scope.rooms[i].id_room === id) {
        //remove rooms
        $scope.rooms.splice(i, 1);
        $scope.totalItems -= 1;
      }
    }
  }


  $scope.deleteRoom = function(id) {

    services.delete('crud', 'borrar_sala', id)
      .then(function(response) {
        console.log(response);
        $scope.setRooms(id);
        //$route.reload();
      });
  };

});
