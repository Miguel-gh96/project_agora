app.controller('new_user', function($scope, $rootScope, AuthenticationService, $sce, $location, services) {

  //we use this scope variable to show succesful updagted message
  $scope.success = false;
  //console.log(dataU_service);
  /////////////Get user////////////////////////////////////

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();
  console.log('nuevo');
  $scope.user = {
    data:{
      name: '',
      last_name: '',
      email: '',
      birth_date:'',
      en_lvl: 'bajo',
      avatar: '',
      selectedCountry: '',
      selectedProvince: '',
      selectedTown: '',
      register_data:new Date(yyyy+"-"+mm+"-"+dd),
      status:'active',
      token:'',
      type:'subscriber',
      pass:'',
      user:''

    },
    error:{
      user:'',
      reg_user:'',
      name: '',
      last_name:'',
      email: '',
      birth_date: '',
      en_lvl: '',
      avatar: '',
      selectedCountry: '',
      selectedProvince:'',
      selectedTown:''
    }

  };



      //automatically upload countries
      load_countries_v1();
      if ($scope.user.data.selectedProvince != '') {
        //console.log($scope.user.data.selectedProvince);
        load_provincias_v1();
        load_poblaciones_v1($scope.user.data.selectedProvince);
      }


  ////////////////////end get user//////////////////////////////////////////

  //when you change the country
  $scope.updateCountry = function() {
    $scope.provinces = null;
    $scope.towns = null;
    load_provincias_v1('');
  };
  //when you change the province
  $scope.updateProvince = function() {
    $scope.towns = null;
    if ($scope.user.data.selectedProvince > 0) {
      //upload town
      load_poblaciones_v1($scope.user.data.selectedProvince);
    }
  };
  //});

  $scope.updateUser = function() {
    //clear errors


    $scope.success = false;
    ///Check country,province,town///////////
    var country = $scope.user.data.selectedCountry;
    var province = $scope.user.data.selectedProvince;
    var town = $scope.user.data.selectedTown;
    var date_birth = $scope.user.data.birth_date;
    var date_reg = $scope.user.data.register_data;

    if (!country) {
      country = 'default_country';
    }

    if (!province) {
      province = 'default_province';
    }

    if (!town) {
      town = 'default_town';
    }
    ///end Check country,province,town///////////
    for(name in $scope.user.error){
          $scope.user.error[name] = "";
      }

    //////check date not empty//////////////////
    if(date_birth){
      date_birth = $scope.getFormattedDate(date_birth);
    }
    if(date_reg){
      date_reg = $scope.getFormattedDate(date_reg);
    }
    //////end check date not empty//////////////
    console.log($rootScope.globals);
    var data_user = {

      user:$scope.user.data.user,
      pass:$scope.user.data.pass,
      name: $scope.user.data.name,
      last_name: $scope.user.data.last_name,
      birth_date:date_birth,
      email: $scope.user.data.email,
      status:$scope.user.data.status,
      avatar:$rootScope.globals.currentUser.avatar_user,
      country: country,
      province: province,
      town: town,
      en_lvl: $scope.user.data.en_lvl,
      type_user:$scope.user.data.type,
      token:$scope.user.data.token,
      reg_user:date_reg
    }

    services.put('crud', 'nuevo_usuario', JSON.stringify({
        data: data_user
      }))
      .then(function(results) {
        console.log(results);
        if (results.success) {
          $scope.success = true;
          console.log(results);
        } else {
          //server control errors
          for(name in $scope.user.error){
            if(results.error[name]){
                $scope.user.error[name] = $sce.trustAsHtml(results.error[name]);
            }
          }

        }
      });

  };

  $scope.validateFormUser = function() {
    if (!$scope.form_user.$valid) {
      angular.forEach($scope.form_user.$error.required, function(field) {
        field.$setDirty();
      });

      return false;
    } else {
      $scope.updateUser();
      return true;
    }
  }



  /******************  COUNTRY  *************************/
  function load_countries_v1() {

    var load_country = {
      load_country: true
    };
    services.post('users', 'load_countries_users', load_country)
      .then(function(response) {
        //console.log(response);
        /*Error, upload xml data*/
        if (response != 'error') {
          services.getXML('./backend/resources/ListOfCountryNamesByName.json')
            .then(function(response) {
              $scope.countries = response;
            });
        } else {
          $scope.countries = response;
        }
      });
  }

  /******************  PROVINCE  *************************/
  function load_provincias_v1() {
    //provinces.id.nombre
    var load_provinces = {
      load_provinces: true
    };
    services.post('users', 'load_provinces_users', load_provinces)
      .then(function(response) {
        //console.log(response);
        /*Error, upload xml data*/
        if (response.provinces != 'error') {
          services.getXML('./backend/resources/provinciasypoblaciones.xml')
            .then(function(response) {
              //console.log(response);
              $scope.provinces = response.lista.provincia;
            });
        } else {
          $scope.provinces = response.provinces;
        }
      });
  }

  /******************  TOWN  *************************/
  function load_poblaciones_v1(prov) {

    var idPoblac = {
      idPoblac: prov
    };
    services.post('users', 'load_towns_users', idPoblac)
      .then(function(response) {
        //console.log(response);
        /*Error, upload xml data*/
        if (response.towns === 'error') {
          services.getXML('./backend/resources/provinciasypoblaciones.xml')
            .then(function(response) {
              $scope.towns = response.lista.provincia[parseInt(prov)].localidades.localidad;
            });
        } else {
          $scope.towns = response.towns;
        }
      });
  }

  //////////////////DatePicker//////////////////////////////////
  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.formats = ['MM/dd/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];
  $scope.popup1 = {
    opened: false
  };

  $scope.getFormattedDate = function(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
  }
  //////////////////end DatePicker//////////////////////////////////


});
