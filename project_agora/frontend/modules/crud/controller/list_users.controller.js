app.controller('list_users', function($scope, AuthenticationService, $location, $sce,$rootScope, /* $location, $http,*/ services) {

  //retrieve employees listing from API
  services.get('crud', 'obtener_usuarios').then(function(response) {
    console.log(response);
    if (response.success) {
      $scope.users = response.list_users;

      $scope.viewby = 5;
      $scope.totalItems = $scope.users.length;
      $scope.currentPage = 1;
      $scope.itemsPerPage = $scope.viewby;
      $scope.maxSize = 5; //Number of pager buttons to show

      $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
      };

      $scope.pageChanged = function() {
        console.log('Page changed to: ' + $scope.currentPage);
      };

      $scope.setItemsPerPage = function(num) {
        $scope.itemsPerPage = num;
        $scope.currentPage = 1; //reset to first paghe
      }

      $scope.setUsers = function(id) {
        for (var i = 0; i < $scope.users.length; i++) {
          if ($scope.users[i].user === id) {
            //remove user
            $scope.users.splice(i, 1);
            $scope.totalItems -= 1;
          }
        }
      }

      //go form
      $scope.toggle = function(modalstate, id) {
        $scope.error_DB = '';
        switch (modalstate) {
          case 'add':
            $location.path('/listar_usuarios/nuevo_usuario');
            break;
          case 'edit':
            console.log(id);
            $location.path('/listar_usuarios/editar_usuario/' + id);
            break;
          case 'remove':
            var isConfirmDelete = confirm('¿Estás seguro de eliminar este usuario?');
            if (isConfirmDelete) {

              //if user selected to delete is  user logged, it alert and if user confimr delete it  redirect to home
              if (id === $rootScope.globals.currentUser.user) {
                var isConfirmDelete2 = confirm('Estás a punto de borrar tu usuario, ¿estás seguro?');
                if (isConfirmDelete2) {
                  services.delete('crud', 'borrar_usuario', id).then(function(response) {
                    console.log(response);
                    if (response.success) {
                      AuthenticationService.ClearCredentials();
                      $location.path('/');
                    } else {
                      $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>" + response.error_message + "</span>")
                    }
                  })
                }
              }

              services.delete('crud', 'borrar_usuario', id).then(function(response) {
                console.log(response);
                if (response.success) {
                  //location.reload();
                  $scope.setUsers(id);
                } else {
                  $scope.error_DB = $sce.trustAsHtml("<span class='col-sm-12 danger1'>" + response.error_message + "</span>")
                }
              })


            }
            break;
          default:
            break;
        }
        //console.log(id);
        //$('#myModal').modal('show');
      }

    } else {
      $location.path('/');
    }

  });



});
