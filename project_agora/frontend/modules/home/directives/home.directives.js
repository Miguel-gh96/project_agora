/* ----------------------------------------------------------- */
/*  1. Superslides Slider
/* ----------------------------------------------------------- */
app.directive('superSlides', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.superslides({
        animation: 'slide',
        play: '10000'
      });

    }
  }
});

/* ----------------------------------------------------------- */
/*  3. Featured Slider
/* ----------------------------------------------------------- */

app.directive('featuredSlider', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.slick({
        dots: true,
        infinite: true,
        speed: 800,
        arrows: false,
        slidesToShow: 1,
        slide: 'div',
        autoplay: true,
        fade: true,
        autoplaySpeed: 5000,
        cssEase: 'linear'
      });
    }
  }
});

/* ----------------------------------------------------------- */
/*  8. BLOG SLIDER
/* ----------------------------------------------------------- */

app.directive('blogSlider', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        }, {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }, {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
      });
    }
  }
});
