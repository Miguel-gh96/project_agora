app.controller('home', function($scope, $uibModal, $rootScope, $routeParams, $location, $http, dataU_service,AuthenticationService ,UserService) {

  if (dataU_service != 0 && dataU_service.error_DB === undefined) {

    //add user in local storage
    UserService.Create(dataU_service.user);

    //add use in cookies
    AuthenticationService.Login(dataU_service.user.email_user, dataU_service.user.pass_user, function(data) {
      //console.log(data.success);
      //console.log(data.message);
      //console.log(data.data);
      if (data.success) {
        AuthenticationService.SetCredentials(dataU_service.user);
      } else {
        console.log("bad");
      }
    });

  } else {
    $scope.items = [];

    $scope.animationsEnabled = true;

    $scope.open = function(size, url, controller) {
      console.log(url + " controller: " + controller);
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: url,
        controller: controller,
        size: size,
        resolve: {
          items: function() {
            return $scope.items;
          }
        }
      });

    };
  }


////////////////////////Jquery function//////////////////////////////////
  /* ----------------------------------------------------------- */
  /*  2. Fixed Top Menubar
  /* ----------------------------------------------------------- */

  $(window).scroll(function() {

    if ($("#navbar-fixed").hasClass("main")) { /* if exist main we are in main */
      //interruptor_home = true;
      if ($(window).scrollTop() > 100) /*or $(window).height() */ {
        $(".navbar-fixed-top").addClass('past-main');
      } else {
        $(".navbar-fixed-top").removeClass('past-main');
      }
    }
  });
/////////////////////end Jquery function////////////////////////////////
});
