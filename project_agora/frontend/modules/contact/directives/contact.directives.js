app.directive('mapcanvas', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.gmap('option', 'zoom');
      element.gmap().bind('init', function(ev, map) {
        element.gmap('addMarker', {
          'position': '38.81951489417874,-0.6009234488010406',
          'bounds': true
        });
        element.gmap('option', 'zoom', 15);
      });

    }
  }
});
