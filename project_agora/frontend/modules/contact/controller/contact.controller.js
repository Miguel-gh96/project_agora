app.controller('contact', function(commonService, $scope, services /*, $location, $http */ ) {
  //commonService.print_menu('');
  //commonService.get_userCookie();
  console.log("Contact");

  $scope.data_contact = {
    inputName: "",
    inputEmail: "",
    inputSubject: "compra",
    inputMessage: "",
    token:"contact_form"
  }


  $scope.validateForm = function() {
    if (!$scope.contact_form.$valid) {
      angular.forEach($scope.contact_form.$error.required, function(field) {
        field.$setDirty();
      });

      return false;
    } else {
      $scope.send_contact();
      return true;
    }
  }

  /////////////Send Contact////////////////////////////
  $scope.send_contact = function() {
    // Disable button while processing
    $('#submitBtn').attr('disabled', true);
    // show ajax loader icon
    $('.ajaxLoader').fadeIn("fast");

    var data = JSON.stringify($scope.data_contact);

    services.post('contact', 'process_contact', data)
      .then(function(results) {
        console.log(results);
        paint(results);
      }, function errorCallback(results) {
        paint("<div class='alert alert-error'>Server error. Tu mensaje no ha sido enviado...</div>");
      });

  };
  ////////////end send_contact ///////////////////////


  ///////////JQuery function//////////////////////////
  function paint(dataString) {
    $("#resultMessage").html(dataString).fadeIn("slow");

    setTimeout(function() {
      $("#resultMessage").fadeOut("slow")
    }, 5000);

    //reset the form
    $('#contact_form')[0].reset();

    // hide ajax loader icon
    $('.ajaxLoader').fadeOut("fast");

    // Enable button after processing
    $('#submitBtn').attr('disabled', false);

  }
  /////////////end function////////////////////////


});
