app.controller('profile_personal', function($scope, $rootScope, commonService, AuthenticationService, $sce, $location, services) {

  //we use this scope variable to show succesful updagted message
  $scope.success = false;

  /////////////Get user////////////////////////////////////

  services.get('users', 'recoger_datos_users', $rootScope.globals.currentUser.user)
    .then(function(results) {
      //console.log(results);
      if (results.success) {

        //change date to create a new Date because if we don't have this, we have problems with ui bootstrap datepicker
        var date = "";
        if(results.user.birthday_user){
          var split_date = results.user.birthday_user.split('/');
          var month = split_date[0];
          var day = split_date[1];
          var year = split_date[2];
          date = new Date(year+"-"+month+"-"+day);
        }



        var province = results.user.province_user;
        var town = results.user.city_user;
        if(province === 'default_province'){
          province = "";
        }
        if(town === 'default_town'){
          town = "";
        }

        $scope.user = {
          data:{
            user: results.user.user,
            name: results.user.name_user,
            last_name: results.user.lastname_user,
            email: results.user.email_user,
            birth_date: date,
            en_lvl: results.user.level_user,
            avatar: results.user.avatar_user,
            selectedCountry: results.user.country_user,
            selectedProvince: province,
            selectedTown: town

          },
          error:{
            name: '',
            last_name:'',
            email: '',
            birth_date: '',
            en_lvl: '',
            avatar: '',
            selectedCountry: '',
            selectedProvince:'',
            selectedTown:''
          }

        };


      } else {
        $location.path('/');
      }


      //automatically upload countries
      load_countries_v1();
      if ($scope.user.data.selectedProvince != '') {
        //console.log($scope.user.data.selectedProvince);
        load_provincias_v1();
        load_poblaciones_v1($scope.user.data.selectedProvince);
      }



    });
  ////////////////////end get user//////////////////////////////////////////


  //when you change the country
  $scope.updateCountry = function() {
    $scope.provinces = null;
    $scope.towns = null;
    load_provincias_v1('');
  };
  //when you change the province
  $scope.updateProvince = function() {
    $scope.towns = null;
    if ($scope.user.data.selectedProvince > 0) {
      //upload town
      load_poblaciones_v1($scope.user.data.selectedProvince);
    }
  };
  //});

  $scope.updateUser = function() {
    //clear errors


    $scope.success = false;
    ///Check country,province,town///////////
    var country = $scope.user.data.selectedCountry;
    var province = $scope.user.data.selectedProvince;
    var town = $scope.user.data.selectedTown;
    var date = $scope.user.data.birth_date;

    if (!country) {
      country = 'default_country';
    }

    if (!province) {
      province = 'default_province';
    }

    if (!town) {
      town = 'default_town';
    }
    ///end Check country,province,town///////////
    for(name in $scope.user.error){
          $scope.user.error[name] = "";
      }

    //////check date not empty//////////////////
    if(date){
      date = $scope.getFormattedDate(date);
    }
    //////end check date not empty//////////////

    var data_user = {
      user:$scope.user.data.user,
      name: $scope.user.data.name,
      last_name: $scope.user.data.last_name,
      email: $scope.user.data.email,
      birth_date:date,
      en_lvl: $scope.user.data.en_lvl,
      country: country,
      province: province,
      town: town,
      avatar:$rootScope.globals.currentUser.avatar_user,
      email_old:$rootScope.globals.currentUser.email_user,
      status_user:$rootScope.globals.currentUser.status_user,
      type_user:$rootScope.globals.currentUser.type_user,
      token:$rootScope.globals.currentUser.token,
      reg_user:$rootScope.globals.currentUser.reg_user
    }

    services.put('users', 'update_users', JSON.stringify({
        data: data_user
      }))
      .then(function(results) {
        if (results.success) {
          $scope.success = true;

          //refresh cookie
          AuthenticationService.ClearCredentials();
          AuthenticationService.SetCredentials(results.user);
          commonService.get_userCookie();

        } else {
          //server control errors
          for(name in $scope.user.error){
            if(results.error[name]){
                $scope.user.error[name] = $sce.trustAsHtml(results.error[name]);
            }
          }

        }
      });

  };

  $scope.validateFormUser = function() {
    if (!$scope.form_user.$valid) {
      angular.forEach($scope.form_user.$error.required, function(field) {
        field.$setDirty();
      });

      return false;
    } else {
      $scope.updateUser();
      return true;
    }
  }



  /******************  COUNTRY  *************************/
  function load_countries_v1() {

    var load_country = {
      load_country: true
    };
    services.post('users', 'load_countries_users', load_country)
      .then(function(response) {
        //console.log(response);
        /*Error, upload xml data*/
        if (response != 'error') {
          services.getXML('./backend/resources/ListOfCountryNamesByName.json')
            .then(function(response) {
              $scope.countries = response;
            });
        } else {
          $scope.countries = response;
        }
      });
  }

  /******************  PROVINCE  *************************/
  function load_provincias_v1() {
    //provinces.id.nombre
    var load_provinces = {
      load_provinces: true
    };
    services.post('users', 'load_provinces_users', load_provinces)
      .then(function(response) {
        //console.log(response);
        /*Error, upload xml data*/
        if (response.provinces != 'error') {
          services.getXML('./backend/resources/provinciasypoblaciones.xml')
            .then(function(response) {
              //console.log(response);
              $scope.provinces = response.lista.provincia;
            });
        } else {
          $scope.provinces = response.provinces;
        }
      });
  }

  /******************  TOWN  *************************/
  function load_poblaciones_v1(prov) {

    var idPoblac = {
      idPoblac: prov
    };
    services.post('users', 'load_towns_users', idPoblac)
      .then(function(response) {
        //console.log(response);
        /*Error, upload xml data*/
        if (response.towns === 'error') {
          services.getXML('./backend/resources/provinciasypoblaciones.xml')
            .then(function(response) {
              $scope.towns = response.lista.provincia[parseInt(prov)].localidades.localidad;
            });
        } else {
          $scope.towns = response.towns;
        }
      });
  }

  //////////////////DatePicker//////////////////////////////////
  $scope.popup1 = {
    opened: false
  };
  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.formats = ['MM/dd/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];
  $scope.popup1 = {
    opened: false
  };

  $scope.getFormattedDate = function(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
  }
  //////////////////end DatePicker//////////////////////////////////


});
