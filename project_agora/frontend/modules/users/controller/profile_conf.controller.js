app.controller('profile_conf', function($scope, AuthenticationService, $sce,$rootScope, /* $location, $http,*/ services) {
  //we use this scope variable to show succesful updagted message
  $scope.success = false;

  $scope.pass = {
    password: "",
    password2: ""
  };
  $scope.pass_error = {
    error_DB: "",
    pass: "",
    conf_pass: ""
  };

  $scope.validatePass = function() {
    if (!$scope.form_user.$valid) {
      angular.forEach($scope.form_user.$error.required, function(field) {
        field.$setDirty();
      });
    } else {
      $scope.updatePass();
    }
  };
  $scope.updatePass = function(pass) {

    var data_pass = JSON.stringify({
      password: $scope.pass.password,
      user:$rootScope.globals.currentUser.user
    });
    //console.log(data_pass);
    services.put('users', 'update_users', data_pass)
    .then(function(results) {
      //console.log(results);
      if (results.success) {
        $scope.success = true;
      } else {
        $scope.success = false;
        //console.log(results.error_message);
      }
    });
  };

});
