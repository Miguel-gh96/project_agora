/*
  url-guide --> http://www.cantangosolutions.com/blog/Easy-File-Upload-Using-DropzoneJS-AngularJs-And-Spring
*/
app.directive('dropzone', function(services) {
  return {
    restrict: 'C',
    link: function(scope, element, attrs) {

      var config = {
        url: './backend/index.php?module=users&function=upload_users',
        maxFilesize:  55000,
        paramName: "file",
        maxThumbnailFilesize: 10,
        parallelUploads: 1,
        addRemoveLinks:true,
        autoProcessQueue: true
      };

      var eventHandlers = {
        'addedfile': function(file) {
          scope.file = file;
          if (this.files[1] != null) {
            this.removeFile(this.files[0]);
          }
          scope.$apply(function() {
            scope.fileAdded = true;
          });
        },

        'success': function(file, response) {
          console.log(response);
        },
        'removedfile':function(file, serverFileName) {
          if(file.xhr.response){
            console.log(file.xhr);
            var data = jQuery.parseJSON(file.xhr.response);
            console.log(file);
            services.post('users','delete_users',JSON.stringify({'filename':data.datos,'delete':true})).then(function(data, status) {

              console.log(data);

            });
          }else{
            ///ERROR///

          }

        }

      };

      dropzone = new Dropzone(element[0], config);

      angular.forEach(eventHandlers, function(handler, event) {
        dropzone.on(event, handler);
      });

    }
  }
});
