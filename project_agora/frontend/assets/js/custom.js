/**
	* SinglePro HTML 1.0
	* Template Scripts
	* Created by WpFreeware Team

	Custom JS

	SCROLL TOP BUTTON


**/

jQuery(function($) {

  /* ----------------------------------------------------------- */
  /*  Wow smooth animation
  /* ----------------------------------------------------------- */
  wow = new WOW({
    animateClass: 'animated',
    offset: 100
  });
  wow.init();

  /* ----------------------------------------------------------- */
  /*  MOBILE MENU ACTIVE CLOSE
  /* ----------------------------------------------------------- */


  /* ----------------------------------------------------------- */
  /*  SCROLL TOP BUTTON
  /* ----------------------------------------------------------- */

  //Check to see if the window is top if not then display button

  $(window).scroll(function() {
    if ($(this).scrollTop() > 300) {
      $('.scrollToTop').fadeIn();
    } else {
      $('.scrollToTop').fadeOut();
    }
  });

  //Click event to scroll to top

  $('.scrollToTop').click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    return false;
  });


});
