define({ "api": [
  {
    "type": "post",
    "url": "backend/index.php?module=contact&function=process_contact",
    "title": "process_contact",
    "name": "process_contact",
    "group": "Contact",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "post",
            "optional": false,
            "field": "token",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n success = 'Tu mensaje ha sido enviado'\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/contact/controller/controller_contact.class.php",
    "groupTitle": "Contact"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=users&function=delete_users",
    "title": "delete_users",
    "name": "delete_users",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "delete",
            "description": "<p>url_avatar</p>"
          }
        ]
      }
    },
    "description": "<p>This is a function used to remove avatar.</p>",
    "permission": [
      {
        "name": "subscriber, admin, associated"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  ['res':true]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/users/controller/controller_users.class.php",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=users&function=load_countries_users",
    "title": "load_countries_users",
    "name": "load_countries_users",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "load_country",
            "description": ""
          }
        ]
      }
    },
    "permission": [
      {
        "name": "subscriber, admin, associated"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  $json\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/users/controller/controller_users.class.php",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=users&function=load_provinces_users",
    "title": "load_provinces_users",
    "name": "load_provinces_users",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "load_provinces",
            "description": ""
          }
        ]
      }
    },
    "permission": [
      {
        "name": "subscriber, admin, associated"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n'provinces': $json\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/users/controller/controller_users.class.php",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=users&function=load_towns_users",
    "title": "load_towns_users",
    "name": "load_towns_users",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "idPoblac",
            "description": ""
          }
        ]
      }
    },
    "permission": [
      {
        "name": "subscriber, admin, associated"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   'towns': $json\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/users/controller/controller_users.class.php",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=users&function=recoger_datos_users",
    "title": "recoger_datos_users",
    "name": "recoger_datos_users",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      }
    },
    "permission": [
      {
        "name": "subscriber, admin, associated"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  'success': true,\n  'user' = {\n      'user':'meganeo',\n      'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',\n      'name_user':'miguel',\n      'lastname_user':'Gomez',\n      'birthday_user':'06/01/1996',\n      'reg_user':'06/01/2016',\n      'email_user':'emai@examp.es',\n      'status_user':'active',\n      'avatar_user':'./backend/media/default-avatar.png',\n      'country_user':'ES',\n      'province_user':'16',\n      'city_user':'Almonacid Del Marquesado',\n      'level_user':'bajo',\n      'type_user':'subscriber',\n      'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/users/controller/controller_users.class.php",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=users&function=update_users",
    "title": "update_users",
    "name": "update_users",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": "<p>user data update</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "subscriber, admin, associated"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  'success': true,\n  'user' = {\n      'user':'meganeo',\n      'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',\n      'name_user':'miguel',\n      'lastname_user':'Gomez',\n      'birthday_user':'06/01/1996',\n      'reg_user':'06/01/2016',\n      'email_user':'emai@examp.es',\n      'status_user':'active',\n      'avatar_user':'./backend/media/default-avatar.png',\n      'country_user':'ES',\n      'province_user':'16',\n      'city_user':'Almonacid Del Marquesado',\n      'level_user':'bajo',\n      'type_user':'subscriber',\n      'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'\n    }\n}",
          "type": "json"
        },
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  'success': true\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/users/controller/controller_users.class.php",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=users&function=upload_users",
    "title": "upload_users",
    "name": "upload_users",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "result_avatar",
            "description": ""
          }
        ]
      }
    },
    "permission": [
      {
        "name": "subscriber, admin, associated"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n '$result_avatar': 'avatar'\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/users/controller/controller_users.class.php",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "backend/index.php?module=crud&function=nuevo_usuario",
    "title": "create_user",
    "name": "create_user",
    "group": "crud",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "data_user",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/crud/controller/controller_crud.class.php",
    "groupTitle": "crud"
  },
  {
    "type": "delete",
    "url": "backend/index.php?module=crud&function=borrar_sala",
    "title": "delete_room",
    "name": "delete_room",
    "group": "crud",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "jsondata Success:",
          "content": "HTTP/1.1 200 OK\n\n \"success\" = true,",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/crud/controller/controller_crud.class.php",
    "groupTitle": "crud"
  },
  {
    "type": "delete",
    "url": "backend/index.php?module=crud&function=borrar_usuario",
    "title": "delete_user",
    "name": "delete_user",
    "group": "crud",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "jsondata Success:",
          "content": "HTTP/1.1 200 OK\n\n \"success\" = true,",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/crud/controller/controller_crud.class.php",
    "groupTitle": "crud"
  },
  {
    "type": "get",
    "url": "backend/index.php?module=crud&function=obtener_salas",
    "title": "get_all_rooms",
    "name": "get_all_rooms",
    "group": "crud",
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n\n \"success\" = true,\n \"list_rooms\" = {\n     \"id_room\": \"id_room\",\n     \"name_room\": \"name_room\",\n     \"topic_room\": \"topic_room\",\n     \"language_room\": \"language_room\",\n     \"num_person_room\": \"num_person_room\",\n     \"creation_date_room\": \"creation_date_room\",\n     \"expire_date_room\": \"expire_date_room\",\n     \"avatar_room\": \"avatar_room\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/crud/controller/controller_crud.class.php",
    "groupTitle": "crud"
  },
  {
    "type": "get",
    "url": "backend/index.php?module=crud&function=obtener_usuarios",
    "title": "get_all_user",
    "name": "get_all_user",
    "group": "crud",
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n\n \"success\" = true,\n \"user\" = {\n     \"user\": \"user\",\n     \"pass_user\": \"pass_user\",\n     \"name_user\": \"name_user\",\n     \"lastname_user\": \"lastname_user\",\n     \"birthday_user\": \"birthday_user\",\n     \"reg_user\": \"reg_user\",\n     \"email_user\": \"email_user\",\n     \"status_user\": \"status_user\",\n     \"avatar_user\": \"avatar_user\",\n     \"country_user\": \"country_user\",\n     \"province_user\": \"province_user\",\n     \"city_user\": \"city_user\",\n     \"level_user\": \"level_user\",\n     \"type_user\": \"type_user\",\n     \"token\": \"token\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/crud/controller/controller_crud.class.php",
    "groupTitle": "crud"
  },
  {
    "type": "get",
    "url": "backend/index.php?module=crud&function=obtener_usuarios",
    "title": "get_user",
    "name": "get_user",
    "group": "crud",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n\n \"success\" = true,\n \"user\" = {\n     \"user\": \"user\",\n     \"pass_user\": \"pass_user\",\n     \"name_user\": \"name_user\",\n     \"lastname_user\": \"lastname_user\",\n     \"birthday_user\": \"birthday_user\",\n     \"reg_user\": \"reg_user\",\n     \"email_user\": \"email_user\",\n     \"status_user\": \"status_user\",\n     \"avatar_user\": \"avatar_user\",\n     \"country_user\": \"country_user\",\n     \"province_user\": \"province_user\",\n     \"city_user\": \"city_user\",\n     \"level_user\": \"level_user\",\n     \"type_user\": \"type_user\",\n     \"token\": \"token\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/crud/controller/controller_crud.class.php",
    "groupTitle": "crud"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=crud&function=actualizar_usuario",
    "title": "update_user",
    "name": "update_user",
    "group": "crud",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/crud/controller/controller_crud.class.php",
    "groupTitle": "crud"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=login&function=exist_user",
    "title": "SignIn",
    "name": "SignIn_enter",
    "group": "login",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "jsondata",
            "description": "<p>Name user and pass user.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   'success' = true,\n   'user' = {\n      'user':'meganeo',\n      'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',\n      'name_user':'miguel',\n      'lastname_user':'Gomez',\n      'birthday_user':'06/01/1996',\n      'reg_user':'06/01/2016',\n      'email_user':'emai@examp.es',\n      'status_user':'active',\n      'avatar_user':'./backend/media/default-avatar.png',\n      'country_user':'ES',\n      'province_user':'16',\n      'city_user':'Almonacid Del Marquesado',\n      'level_user':'bajo',\n      'type_user':'subscriber',\n      'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/login/controller/controller_login.class.php",
    "groupTitle": "login"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=login&function=nuevo",
    "title": "SignUp",
    "name": "SignUp",
    "group": "login",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "jsondata",
            "description": "<p>data new User.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   'success' = true;\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/login/controller/controller_login.class.php",
    "groupTitle": "login"
  },
  {
    "type": "get",
    "url": "backend/index.php?module=login&function=activar&id=4ab160816a8225bcd7fcb3a7fa3b4d42",
    "title": "activate_user",
    "name": "activate_user",
    "group": "login",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "ID",
            "description": "<p>token to activate or recover password.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response activate User:",
          "content": "HTTP/1.1 200 OK\n{\n   'success' = true,\n   'user' = {\n      'user':'meganeo',\n      'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',\n      'name_user':'miguel',\n      'lastname_user':'Gomez',\n      'birthday_user':'06/01/1996',\n      'reg_user':'06/01/2016',\n      'email_user':'emai@examp.es',\n      'status_user':'active',\n      'avatar_user':'./backend/media/default-avatar.png',\n      'country_user':'ES',\n      'province_user':'16',\n      'city_user':'Almonacid Del Marquesado',\n      'level_user':'bajo',\n      'type_user':'subscriber',\n      'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'\n    }\n}",
          "type": "json"
        },
        {
          "title": "jsondata Success-Response recover Password:",
          "content": "HTTP/1.1 200 OK\n{\n   'success' = true,\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/login/controller/controller_login.class.php",
    "groupTitle": "login"
  },
  {
    "type": "get",
    "url": "backend/index.php?module=login&function=exist_email&id=asdasd",
    "title": "recover_pass",
    "name": "recover_pass",
    "group": "login",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Email",
            "description": "<p>Email to confirm account.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response recover Password:",
          "content": "HTTP/1.1 200 OK\n{\n   'success' = true,\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/login/controller/controller_login.class.php",
    "groupTitle": "login"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=login&function=social_facebook",
    "title": "social_facebook",
    "name": "social_facebook",
    "group": "login",
    "permission": [
      {
        "name": "subscriber,associate,admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Pass",
            "description": "<p>Pass to change pass  account.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response recover Password:",
          "content": "HTTP/1.1 200 OK\n{\n   'success' = true,\n   'redirect' = '/salas',\n   'user' = {\n      'user':'meganeo',\n      'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',\n      'name_user':'miguel',\n      'lastname_user':'Gomez',\n      'birthday_user':'06/01/1996',\n      'reg_user':'06/01/2016',\n      'email_user':'emai@examp.es',\n      'status_user':'active',\n      'avatar_user':'./backend/media/default-avatar.png',\n      'country_user':'ES',\n      'province_user':'16',\n      'city_user':'Almonacid Del Marquesado',\n      'level_user':'bajo',\n      'type_user':'subscriber',\n      'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/login/controller/controller_login.class.php",
    "groupTitle": "login"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=login&function=twitter_return",
    "title": "twitter_return",
    "name": "twitter_return",
    "group": "login",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "user",
            "description": "<p>User data.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response recover Password:",
          "content": "HTTP/1.1 200 OK\n{\n   'success' = true,\n   'redirect' = '/salas',\n   'user' = {\n      'user':'meganeo',\n      'pass_user':'kLZrvL4LbTMW9gKy7MkxW6Et7Sw51ia',\n      'name_user':'miguel',\n      'lastname_user':'Gomez',\n      'birthday_user':'06/01/1996',\n      'reg_user':'06/01/2016',\n      'email_user':'emai@examp.es',\n      'status_user':'active',\n      'avatar_user':'./backend/media/default-avatar.png',\n      'country_user':'ES',\n      'province_user':'16',\n      'city_user':'Almonacid Del Marquesado',\n      'level_user':'bajo',\n      'type_user':'subscriber',\n      'token':'4ab160816a8225bcd7fcb3a7fa3b4d42'\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/login/controller/controller_login.class.php",
    "groupTitle": "login"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=login&function=exist_email",
    "title": "update_pass",
    "name": "update_pass",
    "group": "login",
    "permission": [
      {
        "name": "subscriber,associate,admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": "<p>Pass and token  to change pass  account.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "jsondata Success-Response recover Password:",
          "content": "HTTP/1.1 200 OK\n{\n   'success' = true\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/login/controller/controller_login.class.php",
    "groupTitle": "login"
  },
  {
    "type": "get",
    "url": "backend/index.php?module=rooms&function=count_rooms",
    "title": "count_rooms",
    "name": "count_rooms",
    "group": "rooms",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "jsondata Response:",
          "content": "    HTTP/1.1 200 OK\n{\n     \"$total_pages\" : \"total_pages\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/rooms/controller/controller_rooms.class.php",
    "groupTitle": "rooms"
  },
  {
    "type": "post",
    "url": "backend/index.php?module=rooms&function=recoger_datos",
    "title": "recoger_datos",
    "name": "recoger_datos",
    "group": "rooms",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": "<p>content p(numPages) busqueda</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "jsondata Response:",
          "content": " HTTP/1.1 200 OK\n{\n  \"josndata\" = {\n      \"id_room\": \"id_room\",\n      \"name_room\": \"name_room\",\n      \"topic_room\": \"topic_room\",\n      \"language_room\": \"language_room\",\n      \"num_person_room\": \"num_person_room\",\n      \"creation_date_room\": \"creation_date_room\",\n      \"expire_date_room\": \"expire_date_room\",\n      \"avatar_room\": \"avatar_room\"\n   }\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "backend/modules/rooms/controller/controller_rooms.class.php",
    "groupTitle": "rooms"
  }
] });
